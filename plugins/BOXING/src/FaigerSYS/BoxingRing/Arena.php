<?php
namespace FaigerSYS\BoxingRing;

use pocketmine\scheduler\PluginTask;

use pocketmine\Player;

use pocketmine\item\Item;

use pocketmine\math\Vector3;
use pocketmine\level\Position;
use pocketmine\level\Location;

use pocketmine\event\Listener;
use pocketmine\event\player\PlayerQuitEvent;
use pocketmine\event\player\PlayerDeathEvent;
use pocketmine\event\player\PlayerMoveEvent;
use pocketmine\event\player\PlayerCommandPreprocessEvent;

class Arena extends PluginTask implements Listener {
	
	const TYPE_WAITING = 0;
	const TYPE_COUNTDOWN = 1;
	const TYPE_PLAYING = 2;
	
	/** @var object */
	protected $economy;
	
	/** @var int */
	protected $minX, $minY, $minZ, $maxX, $maxY, $maxZ;
	
	/** @var string */
	protected $level_name;
	
	/** @var array */
	protected $spawn1, $spawn2;
	
	/** @var Location[] */
	protected $spawn_cache;
	
	/** @var \pocketmine\tile\Sign */
	protected $sign;
	
	/** @var Vector3 */
	protected $sign_pos;
	
	/** @var string */
	protected $sign_level_name;
	
	/** @var int */
	protected $active_type = self::TYPE_WAITING;
	
	/** @var Player[] */
	protected $players = [];
	
	/** @var int */
	protected $time_cache;
	
	public function __construct(BoxingRing $owner, array $ring_data, array $sign_data) {
		list($this->minX, $this->minY, $this->minZ) = $ring_data['minPos'];
		list($this->maxX, $this->maxY, $this->maxZ) = $ring_data['maxPos'];
		$level = $owner->getServer()->getLevelByName($this->level_name = $ring_data['levelName']);
		
		$spawn1 = $ring_data['spawn1'];
		$spawn1[] = $level;
		$spawn2 = $ring_data['spawn2'];
		$spawn2[] = $level;
		
		$this->spawn1 = new Location(...$spawn1);
		$this->spawn2 = new Location(...$spawn2);
		$this->spawn_cache = [$this->spawn1, $this->spawn2];
		
		list($signX, $signY, $signZ, $this->sign_level_name) = $sign_data;
		$this->sign_pos = new Vector3($signX, $signY, $signZ);
		
		$this->economy = $owner->getServer()->getPluginManager()->getPlugin('aEconomy');
		
		parent::__construct($owner);
		
		$owner->getServer()->getPluginManager()->registerEvents($this, $owner);
		$owner->getServer()->getScheduler()->scheduleRepeatingTask($this, 20);
	}
	
	public function onRun($tick) {
		if ($this->active_type === self::TYPE_WAITING) {
			
			if ($sign = $this->getSign()) {
				$this->getOwner()->processSign($sign, 'free');
			}
			
			if (count($this->players)) {
				foreach ($this->players as $data) {
					list($player, $spawn) = $data;
					
					if (!$this->isOnArena($player)) {
						$player->teleport($spawn);
					}
					
					$this->getOwner()->processMessage($player, 'waiting');
				}
			}
			
		} elseif ($this->active_type === self::TYPE_COUNTDOWN) {
			
			if ($sign = $this->getSign()) {
				$this->getOwner()->processSign($sign, 'busy');
			}
			
			foreach ($this->players as $name => $data) {
				list($player, $spawn) = $data;
				
				if (!$this->isOnArena($player)) {
					$player->teleport($spawn);
				}
			}
			
			$left = $this->time_cache - time();
			if ($left > 0) {
				foreach ($this->players as $data) {
					list($player) = $data;
					$this->getOwner()->processMessage($player, 'countdown', ['%LEFT%' => $left]);
				}
			} else {
				$this->active_type = self::TYPE_PLAYING;
				$this->time_cache = time() + 60;
				foreach ($this->players as $data) {
					list($player) = $data;
					$this->getOwner()->processMessage($player, 'start-battle');
				}
			}
			
		} elseif ($this->active_type === self::TYPE_PLAYING) {
			
			$left = $this->time_cache - time();
			if ($left > 0) {
				foreach ($this->players as $data) {
					list($player, $spawn) = $data;
					
					if (!$this->isOnArena($player)) {
						$player->teleport($spawn);
					}
					
					$this->getOwner()->processMessage($player, 'on-battle', ['%LEFT%' => $left]);
				}
			} else {
				$this->active_type = self::TYPE_WAITING;
				
				foreach ($this->players as $data) {
					list($player, $b, $inv_data) = $data;
					
					$player->teleport($player->getSpawn());
					$player->getInventory()->setContents(self::getItemsFromArray($inv_data));
					
					$this->economy->addMoney($player, 100);
					
					$this->getOwner()->processMessage($player, 'tie');
				}
				
				$this->players = [];
				$this->spawn_cache = [$this->spawn1, $this->spawn2];
				$this->time_cache = null;
			}
		}
	}
	
	public function onQuit(PlayerQuitEvent $e) {
		$loser = $e->getPlayer();
		if (isset($this->players[$loser->getName()])) {
			if ($this->active_type === self::TYPE_WAITING) {
				list($a, $b, $inv_data) = $this->players[$loser->getName()];
				
				$this->players = [];
				$player->getInventory()->setContents(self::getItemsFromArray($inv_data));
				
				$player->setFood($player->getMaxFood());
				$player->setHealth($player->getMaxHealth());
				$this->economy->addMoney($player, 100);
				
				$this->spawn_cache = [$this->spawn1, $this->spawn2];
				
			} else {
				
				$winner = $this->getEnemy($loser);
				$this->processEnd($winner, $loser);
				
			}
		}
	}
	
	public function onKill(PlayerDeathEvent $e) {
		$loser = $e->getPlayer();
		if (isset($this->players[$loser->getName()])) {
			$e->setKeepInventory(true);
			
			$winner = $this->getEnemy($loser);
			$this->processEnd($winner, $loser);
		}
	}
	
	/**
	 * @priority LOW
	 */
	public function onMove(PlayerMoveEvent $e) {
		$player = $e->getPlayer();
		if ($this->active_type !== self::TYPE_PLAYING && isset($this->players[$player->getName()])) {
			$e->setCancelled(true);
		}
	}
	
	/**
	 * @priority LOW
	 */
	public function onCommand(PlayerCommandPreprocessEvent $e) :bool{
		$player = $e->getPlayer();
		if ($this->active_type !== self::TYPE_WAITING && isset($this->players[$player->getName()])) {
			$this->getOwner()->processMessage($player, 'command-fail');
			$e->setCancelled(true);
		}
	}
	
	protected function processEnd(Player $winner, Player $loser) {
		$this->active_type = self::TYPE_WAITING;
		list($a, $b, $winner_inv) = $this->players[$winner->getName()];
		list($a, $b, $loser_inv) = $this->players[$loser->getName()];
		$this->players = [];
		$this->spawn_cache = [$this->spawn1, $this->spawn2];
		$this->time_cache = null;
		
		$winner_name = $winner->getName();
		$loser_name = $loser->getName();
		
		$winner->setFood($winner->getMaxFood());
		$loser->setFood($loser->getMaxFood());
		$winner->setHealth($winner->getMaxHealth());
		$loser->setHealth($loser->getMaxHealth());
		
		$winner->teleport($winner->getSpawn());
		$loser->teleport($loser->getSpawn());
		
		$this->economy->addMoney($winner, 200);
		
		$winner->getInventory()->setContents(self::getItemsFromArray($winner_inv));
		$loser->getInventory()->setContents(self::getItemsFromArray($loser_inv));
		
		$this->getOwner()->processMessage($winner, 'win', ['%ENEMY%' => $loser_name]);
		$this->getOwner()->processMessage($loser, 'lose', ['%ENEMY%' => $winner_name]);
		
		$this->getOwner()->processBroadcast('end', ['%WINNER%' => $winner_name, '%LOSER%' => $loser_name]);
	}
	
	public function processExit(Player $player) {
		if ($this->isPlayerOnArena($player)) {
			if ($this->active_type === self::TYPE_WAITING) {
				list($a, $b, $inv_data) = $this->players[$player->getName()];
				
				$this->players = [];
				$this->spawn_cache = [$this->spawn1, $this->spawn2];
				
				$player->setFood($player->getMaxFood());
				$player->setHealth($player->getMaxHealth());
				$this->economy->addMoney($player, 100);
				
				$player->getInventory()->setContents(self::getItemsFromArray($inv_data));
				
				$player->teleport($player->getSpawn());
				$this->getOwner()->processMessage($player, 'exit-command');
				
			} else {
				// Imposible
				// $this->getOwner()->processMessage($player, '...');
			}
		} else {
			$this->getOwner()->processMessage($player, 'exit-command-fail');
		}
	}
	
	public function getActiveType() {
		return $this->active_type;
	}
	
	public function canAddPlayer(Player $player = null) {
		return ((!$player || $this->isPlayerOnArena($player)) || count($this->players) < 2);
	}
	
	public function addPlayer(Player $player) {
		if ($this->canAddPlayer($player)) {
			if ($this->economy->myMoney($player) >= 100) {
				$spawn = array_shift($this->spawn_cache);
				$player->teleport($spawn);
				
				$inv = $player->getInventory();
				$inv_data = self::inventoryToArray($inv);
				$inv->clearAll();
				
				$this->players[$player->getName()] = [$player, $spawn, $inv_data];
				
				$player->setFood($player->getMaxFood());
				$player->setHealth($player->getMaxHealth());
				
				$this->economy->reduceMoney($player, 100);
				
				if ($this->active_type === self::TYPE_WAITING && count($this->players) === 2) {
					
					$this->active_type = self::TYPE_COUNTDOWN;
					$this->time_cache = time() + 5;
					
					$enemy = $this->getEnemy($player);
					$this->getOwner()->processMessage($enemy, 'enter-other', ['%ENEMY%' => $player->getName()]);
					
				}
				
				$this->getOwner()->processMessage($player, 'enter-self');
				
				return true;
			} else {
				$this->getOwner()->processMessage($player, 'enter-fail-money');
			}
		} else {
			$this->getOwner()->processMessage($player, 'enter-fail');
		}
		
		return false;
	}
	
	public function isPlayerOnArena(Player $player) {
		return isset($this->players[$player->getName()]);
	}
	
	public function getEnemy(Player $player) {
		if (isset($this->players[$player->getName()])) {
			$players = $this->players;
			while ($data = array_shift($players)) {
				list($enemy) = $data;
				if ($enemy !== $player) {
					return $enemy;
				}
			}
		}
	}
	
	public function getSign() {
		if (!$this->sign || $this->sign->closed) {
			if ($level = $this->getOwner()->getServer()->getLevelByName($this->sign_level_name)) {
				$this->sign = $level->getTile($this->sign_pos);
			} else {
				$this->sign = null;
			}
		}
		
		return $this->sign;
	}
	
	public function isOnArena(Position $pos) {
		$x = $pos->getX();
		$y = $pos->getY();
		$z = $pos->getZ();
		$level_name = $pos->getLevel()->getName();
		
		return (
			$x >= $this->minX && $x <= $this->maxX &&
			$y >= $this->minY && $y <= $this->maxY &&
			$z >= $this->minZ && $z <= $this->maxZ &&
			$level_name === $this->level_name
		);
	}
	
	public static function inventoryToArray($inv) {
		if ($inv !== null) {
			$items = $inv->getContents();
			$data = [];
			foreach ($items as $slot => $item) {
				$id = $item->getId();
				$meta = $item->getDamage();
				$count = $item->getCount();
				$tag = $item->getCompoundTag();
				$data[$slot] = ['id' => $id, 'meta' => $meta, 'count' => $count, 'tag' => $tag];
			}
			return $data;
		} else {
			return false;
		}
	}
	
	public static function getItemsFromArray($data) {
		$items = [];
		if (!is_array($data))
			return [];
		
		foreach ($data as $slot => $itemData) {
			$item = new Item($itemData['id'], $itemData['meta'], $itemData['count']);
			$item->setCompoundTag($itemData['tag']);
			$items[$slot] = $item;
		}
		
		return $items;
	}
	
	public function onDisable() {
		$this->active_type = self::TYPE_WAITING;
		
		foreach ($this->players as $data) {
			list($player, $b, $inv_data) = $data;
			
			$player->teleport($player->getSpawn());
			$player->getInventory()->setContents(self::getItemsFromArray($inv_data));
			
			$this->economy->addMoney($player, 100);
			
			$this->getOwner()->processMessage($player, 'tie');
		}
		
		$this->players = null;
		$this->spawn_cache = null;
		$this->time_cache = null;
	}
	
}
