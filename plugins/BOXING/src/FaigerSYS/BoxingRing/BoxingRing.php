<?php
namespace FaigerSYS\BoxingRing;

use pocketmine\plugin\PluginBase;

use pocketmine\utils\TextFormat as CLR;

use pocketmine\Player;

use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\command\ConsoleCommandSender;

use pocketmine\item\Item;

use pocketmine\tile\Sign as SignTile;

use pocketmine\event\Listener;
use pocketmine\event\player\PlayerInteractEvent;

class BoxingRing extends PluginBase implements Listener {
	
	const PREFIX = CLR::BOLD . CLR::GOLD . '[' . CLR::AQUA . 'BoxingRing' . CLR::GOLD . ']' . CLR::RESET . CLR::GRAY . ' ';
	
	/** @var float[] */
	private $pos1, $pos2, $spos1, $spos2;
	
	/** @var array */
	private $ring_data, $sign_data;
	
	/** @var string|null */
	private $set_sign_mode;
	
	/** @var Arena|null */
	private $arena;
	
	/** @var array */
	private $messages, $broadcasts, $sign_texts;
	
	public function onEnable() {
		$this->getLogger()->info(CLR::GREEN . 'BoxingRing загружается...');
		
		$this->prepareData();
		
		if (!$this->getServer()->getPluginManager()->getPlugin('aEconomy')) {
			$this->getLogger()->error('Для работы необходим aEconomy! Остановка загрузки...');
			$this->setEnabled(false);
			return;
		}
		
		if ($this->ring_data && $this->sign_data) {
			$this->arena = new Arena($this, $this->ring_data, $this->sign_data);
		} else {
			$this->getLogger()->warning('Вы ещё не настроили арену и табличку!');
		}
		
		$this->getServer()->getPluginManager()->registerEvents($this, $this);
		
		$this->getLogger()->info(CLR::GREEN . 'BoxingRing загружен!');
	}
	
	public function onCommand(CommandSender $sender, Command $command, string $label, array $args) :bool{
		if ($sender instanceof ConsoleCommandSender) {
			$sender->sendMessage(self::PREFIX . 'Выполните эту команду в игре!');
			return false;
		}
		
		$cmd = mb_strtolower(array_shift($args));
		switch ($cmd) {
			case 'exit':
			case 'выход':
				if ($this->arena) {
					$this->arena->processExit($sender);
				} else {
					$sender->sendMessage(self::PREFIX . 'Арена не найдена! Выполните настройку перед использованием');
				}
				break;
			
			case 'pos1':
			case 'p1':
				if (!$sender->hasPermission('boxingring.command.setup')) break;
				$this->pos1 = [$sender->getFloorX(), $sender->getFloorY(), $sender->getFloorZ()];
				$sender->sendMessage(self::PREFIX . 'Позиция 1 установлена успешно!');
				break;
			
			case 'pos2':
			case 'p2':
				if (!$sender->hasPermission('boxingring.command.setup')) break;
				$this->pos2 = [$sender->getFloorX(), $sender->getFloorY(), $sender->getFloorZ()];
				$sender->sendMessage(self::PREFIX . 'Позиция 2 установлена успешно!');
				break;
			
			case 'spos1':
			case 'sp1':
				if (!$sender->hasPermission('boxingring.command.setup')) break;
				if ($this->pos1 && $this->pos2) {
					$this->spos1 = [$sender->getX(), $sender->getY(), $sender->getZ(), $sender->getYaw(), $sender->getPitch()];
					$sender->sendMessage(self::PREFIX . 'Точка спавна 1 установлена успешно!');
				} else {
					$sender->sendMessage(self::PREFIX . 'Сначала установите все точки ринга!');
				}
				break;
			
			case 'spos2':
			case 'sp2':
				if (!$sender->hasPermission('boxingring.command.setup')) break;
				if ($this->pos1 && $this->pos2) {
					$this->spos2 = [$sender->getX(), $sender->getY(), $sender->getZ(), $sender->getYaw(), $sender->getPitch()];
					$sender->sendMessage(self::PREFIX . 'Точка спавна 2 установлена успешно!');
				} else {
					$sender->sendMessage(self::PREFIX . 'Сначала установите все точки ринга!');
				}
				break;
			
			case 'setring':
			case 'sr':
				if (!$sender->hasPermission('boxingring.command.setup')) break;
				if ($this->pos1 && $this->pos2 && $this->spos1 && $this->spos2) {
					list($x1, $y1, $z1) = $this->pos1;
					list($x2, $y2, $z2) = $this->pos2;
					$min = [min($x1, $x2), min($y1, $y2), min($z1, $z2)];
					$max = [max($x1, $x2), max($y1, $y2), max($z1, $z2)];
					
					$this->ring_data = [
						'minPos' => $min,
						'maxPos' => $max,
						'spawn1' => $this->spos1,
						'spawn2' => $this->spos2,
						'levelName' => $sender->getLevel()->getName()
					];
					
					$sender->sendMessage(self::PREFIX . 'Арена успешно установлена! Перезагрузите сервер для применения изменений');
				} else {
					$sender->sendMessage(self::PREFIX . 'Сначала установите табличку и все точки ринга/спавна!');
				}
				break;
			
			case 'setsign':
			case 'ss':
				if (!$sender->hasPermission('boxingring.command.setup')) break;
				$this->set_sign_mode = $sender->getName();
				$sender->sendMessage(self::PREFIX . 'Нажмите на табличку для установки...');
				break;
		}
		
		$this->processMessage($sender, 'no-command');

		return true;
	}
	
	public function processMessage(Player $player, string $id, array $replaces = null) {
		if (($data = $this->messages[$id] ?? null) && is_array($data)) {
			list($type, $message) = $data;
			
			if ($replaces) {
				$message = strtr($message, $replaces);
			}
			
			switch ($type) {
				case 'popup':
					$player->sendPopup($message);
					break;
				case 'tip':
					$player->sendTip($message);
					break;
				default:
					$player->sendMessage($message);
					break;
			}
			
		}
	}
	
	public function processBroadcast(string $id, array $replaces = null) {
		if (($data = $this->broadcasts[$id] ?? null) && is_array($data)) {
			list($type, $message) = $data;
			
			if ($replaces) {
				$message = strtr($message, $replaces);
			}
			
			switch ($type) {
				case 'popup':
					$this->getServer()->broadcastPopup($message);
					break;
				case 'tip':
					$this->getServer()->broadcastTip($message);
					break;
				default:
					$this->getServer()->broadcastMessage($message);
					break;
			}
			
		}
	}
	
	public function processSign(SignTile $sign, string $id, array $replaces = null) {
		if ($text = $this->sign_texts[$id] ?? null) {
			if ($replaces) {
				$text = strtr($text, $replaces);
			}
			$text = explode("\n", $text);
			
			$sign->setText(...$text);
		}
	}
	
	/**
	 * @priority HIGH
	 */
	public function onTouch(PlayerInteractEvent $e) {
		$player = $e->getPlayer();
		$block = $e->getBlock();
		if ($block->getId() === Item::SIGN_POST || $block->getId() === Item::WALL_SIGN) {
			$data = [$block->getX(), $block->getY(), $block->getZ(), $block->getLevel()->getName()];
			
			if ($this->set_sign_mode && $this->set_sign_mode === $player->getName()) {
				$this->sign_data = $data;
				$this->set_sign_mode = null;
				$player->sendMessage(self::PREFIX . 'Табличка установлена! Для применения изменений перезагрузите сервер');
			} elseif ($this->arena && $data === $this->sign_data) {
				$this->arena->addPlayer($player);
			}
		}
	}
	
	private function prepareData() {
		@mkdir($path = $this->getDataFolder());
		$defaultConfig = stream_get_contents($this->getResource($file = 'settings.yml'));
		$defaultData = yaml_parse($defaultConfig);
		if (!file_exists($s_path = $path . $file)) {
			file_put_contents($s_path, $defaultConfig);
			$data = $defaultData;
		} else {
			$newData = @yaml_parse(file_get_contents($s_path));
			if (!is_array($newData) || empty($newData)) {
				$this->getLogger()->warning('Файл с настройками был повреждён. Он будет ввостановлен в начальный вид');
				file_put_contents($s_path, $defaultConfig);
				$data = $defaultData;
			} else {
				$data = array_replace_recursive($defaultData, $newData);
			}
		}
		$this->messages = $data['messages'] + $data['notices'];
		$this->broadcasts = $data['broadcasts'];
		$this->sign_texts = $data['sign'];
		
		$data = @json_decode(file_get_contents($path . 'data.json'), true) ?: [];
		$this->ring_data = $data['ring'] ?? null;
		$this->sign_data = $data['sign'] ?? null;
	}
	
	public function onDisable() {
		if ($this->arena) {
			$this->arena->onDisable();
		}
		file_put_contents($this->getDataFolder() . 'data.json', json_encode(['ring' => $this->ring_data, 'sign' => $this->sign_data]));
	}
	
}
