<?php
namespace FaigerSYS\MIE_Protector;

use pocketmine\plugin\PluginBase;

use pocketmine\utils\TextFormat as CLR;

use pocketmine\event\Listener;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\event\block\BlockBreakEvent;

use pocketmine\tile\ItemFrame;

use FaigerSYS\MapImageEngine\item\FilledMap;

class MIE_Protector extends PluginBase implements Listener {
	
	public function onEnable() {
		$this->getLogger()->info(CLR::GOLD . 'MIE_Protector загружается...');
		
		$this->getServer()->getPluginManager()->registerEvents($this, $this);
		
		$this->getLogger()->info(CLR::GOLD . 'MIE_Protector загружен!');
	}
	
	/**
	 * @priority LOW
	 */
	public function onRotate(PlayerInteractEvent $e) {
		if (!$e->isCancelled()) {
			$block = $e->getBlock();
			$frame = $block->getLevel()->getTile($block);
			if ($frame instanceof ItemFrame && $frame->getItem() instanceof FilledMap && !$e->getPlayer()->hasPermission('mapimageengine.bypassprotect')) {
				$e->setCancelled(true);
			}
		}
	}
	
	/**
	 * @priority LOW
	 */
	public function onDestroy(BlockBreakEvent $e) {
		if (!$e->isCancelled()) {
			$block = $e->getBlock();
			$frame = $block->getLevel()->getTile($block);
			if ($frame instanceof ItemFrame && $frame->getItem() instanceof FilledMap && !$e->getPlayer()->hasPermission('mapimageengine.bypassprotect')) {
				$e->setCancelled(true);
			}
		}
	}
	
}
