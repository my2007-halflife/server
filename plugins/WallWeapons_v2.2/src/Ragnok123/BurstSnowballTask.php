<?php

namespace Ragnok123;

use pocketmine\entity\Entity;
use pocketmine\entity\Projectile;
use pocketmine\event\entity\ProjectileLaunchEvent;
use pocketmine\nbt\tag\CompoundTag;
use pocketmine\nbt\tag\DoubleTag;
use pocketmine\nbt\tag\FloatTag;
use pocketmine\nbt\tag\ListTag;
use pocketmine\Player;
use pocketmine\plugin\Plugin;
use pocketmine\scheduler\PluginTask;

class BurstSnowballTask extends PluginTask {

    /**
     * @var Player $player
     */
    private $player;

    public function __construct(WallWeapons $owner, Player $player) {
        parent::__construct($owner);
        $this->player = $player;
    }

    /**
     * Actions to execute when run
     *
     * @param int $currentTick
     *
     * @return void
     */
    public function onRun(int $currentTick) {
        $nbt = new CompoundTag("", [
            "Pos" => new ListTag( "Pos", [
                new DoubleTag( "", $this->player->x),
                new DoubleTag( "", $this->player->y + $this->player->getEyeHeight()),
                new DoubleTag( "", $this->player->z)
            ]),
            "Motion" => new ListTag("Motion", [
                new DoubleTag( "",
                    - \sin($this->player->yaw / 180 * M_PI) *
                    \cos ($this->player->pitch / 180 * M_PI)),
                new DoubleTag( "",
                    - \sin($this->player->pitch / 180 * M_PI )),
                new DoubleTag("",
                    \cos($this->player->yaw / 180 * M_PI)
                    *\cos ( $this->player->pitch / 180 * M_PI)
                )
            ]),
            "Rotation" => new ListTag("Rotation", [
                new FloatTag ( "", $this->player->yaw),
                new FloatTag ( "", $this->player->pitch)
            ])
        ]);

        $f = 1.5;
        $snowball = Entity::createEntity("Snowball", $this->player->level, $nbt, $this->player);
        $snowball->setMotion($snowball->getMotion()->multiply($f));

        if ($snowball instanceof Projectile) {
            $this->getOwner()->server->getPluginManager ()->callEvent ( $projectileEv = new ProjectileLaunchEvent ( $snowball ) );
            if ($projectileEv->isCancelled ()) {
                $snowball->kill ();
            } else {

                $this->getOwner()->object_hash [spl_object_hash ( $snowball )] = 1;
                $snowball->spawnToAll ();
            }
        } else {
            $this->getOwner()->object_hash [spl_object_hash ( $snowball )] = 1;
            $snowball->spawnToAll ();
        }
    }
}

?>