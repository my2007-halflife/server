<?php

/* 
* WallWeapons, gun plugin for MCPE, that running on PocketMine-MP, ImagicalMine-IM and Genisys.
 * Copyright (C) 2016 Ragnok123 <gebab.urumci.kv@gmail.com>
 * * This program is free software: you can redistribute it and/or modify * it under the terms of the GNU General Public License as published by * the Free Software Foundation, either version 3 of the License, or * (at your option) any later version.
 * * This program is distributed in the hope that it will be useful, * but WITHOUT ANY WARRANTY; without even the implied warranty of * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the * GNU General Public License for more details. 
* * You should have received a copy of the GNU General Public License * along with this program. If not, see <http://www.gnu.org/licenses/>. 
*/
namespace Ragnok123;

use pocketmine\entity\Projectile;
use pocketmine\plugin\PluginBase;
use pocketmine\entity\Entity;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\Player;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\nbt\tag\CompoundTag;
use pocketmine\nbt\tag\ListTag;
use pocketmine\nbt\tag\DoubleTag;
use pocketmine\nbt\tag\FloatTag;
use pocketmine\item\Item;
use pocketmine\event\player\PlayerItemHeldEvent;
use pocketmine\event\entity\EntityDamageEvent;
use pocketmine\event\entity\ProjectileLaunchEvent;
use pocketmine\scheduler\CallbackTask;

class WallWeapons extends PluginBase implements Listener {

    /**
     * @var \pocketmine\Server $server
     */
    private $server;

    public $object_hash = [];

	public function onEnable() {
		$this->getServer ()->getPluginManager ()->registerEvents ( $this, $this );
	}

    public function onCommand(CommandSender $entity, Command $cmd, string $label, array $args): bool {
        switch ($cmd->getName()) {
            case "weaps":
			if($entity Instanceof Player) {
				$entity->sendMessage("§e         --===[WallWeapons]===--            ");
          $entity->sendMessage("§6Changelog:                                             ");
          $entity->sendMessage("§cVersion 1.0: Created plugin                 ");
          $entity->sendMessage("§cVersion 1.1: Added new weapon        ");
          $entity->sendMessage("§cVersion 1.2: Fixed bug with Asiimov and multiply ammo");
          $entity->sendMessage("§cVersion 1.3: Fixed bugs again             ");
          $entity->sendMessage("§cVersion 1.4: New ammunation system         ");
          $entity->sendMessage("§cVersion 2.0: Rewrited plugin to PHP7 and api 2.0.0 \n§cAdded new weapons \n§cNow if you choose weapon, plugin sending popup message \n§cChanged ammunation system");
          $entity->sendMessage("§cVersion 2.1: Fixed AK-47 bug");
          $entity->sendMessage("§7Current version of WallWeapons: 2.2");
          $entity->sendMessage("§aVersion 2.2: Added TNT and GNU license");
          $entity->sendMessage("§aVersion 2.3: Remove CallbackTask");
          $entity->sendMessage("§e         --===[WallWeapons]===--            ");
			}
			break;
		}
	} 

    public function onItemHeld(PlayerItemHeldEvent $event){
	    $player = $event->getPlayer();
	    $item = $player->getInventory()->getItemInHand();
            if($item->getId() == 290){
                $player->sendPopup("§6Макаров");
            }
	        if($item->getId() == 269){
	            $player->sendPopup("§6Глок 19"); 
	        }
	        if($item->getId() == 270){
	            $player->sendPopup("§6Беретта 92"); 
	        }
	        if($item->getId() == 271){
	            $player->sendPopup("§6Desert Eagle"); 
	        }
	        /* shotguns */
	        if($item->getId() == 291){
	            $player->sendPopup("§cДробовик 870 Express"); 
	        }
	        if($item->getId() == 273){
	            $player->sendPopup("§cДробовик Arms Defender"); 
	        }
	        if($item->getId() == 274){
	            $player->sendPopup("§cДробовик 88 Security"); 
	        }
	        if($item->getId() == 275){
	            $player->sendPopup("§cДробовик 500 Tactical Persuader"); 
	        }        
	        /* puljemjoty */
	        if($item->getId() == 292){
	            $player->sendPopup("§aM4A1"); 
	        }
	        if($item->getId() == 256){
	            $player->sendPopup("§aАК-47"); 
	        }
	        if($item->getId() == 257){
	            $player->sendPopup("§aАВП 'Азимов'"); 
	        }
	        if($item->getId() == 258){
	            $player->sendPopup("Автомат §AUG"); 
	        }
	        /* AWP */
	        if($item->getId() == 293){
	            $player->sendPopup("§eАВП Dragon Lore"); 
	        }
	        if($item->getId() == 277){
	            $player->sendPopup("§eВинтовка AS50"); 
	        }    
	        if($item->getId() == 278){
	            $player->sendPopup("§eSV98"); 
	        }        	         
           if($item->getId() == 46){
              $player->sendPopup("Базука §eRPG Launcher");
           }
           if($item->getId() == 268){
              $player->sendPopup("§cЛом");
           }
           if($item->getId() == 283){
              $player->sendPopup("§eДубинка §b'ГО'");
           }
           if($item->getId() == 272){
              $player->sendPopup("§eНож"); //для текстур из кс 1.6 надо найти 
           }
           if($item->getId() == 289){
              $player->sendPopup("§2§lБоеприпасы§r");
           }
        }
	
	public function ontap(PlayerInteractEvent $event){
		$player = $event->getPlayer();
		$item = $player->getInventory()->getItemInHand();
		if($item->getId() == 290){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		}
	}
	
						}
						elseif($item->getId() == 270){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		}
	}
	
						}
           elseif($item->getId() == 256){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		}
	}
	
						}
						elseif($item->getId() == 271){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		}
	}
	
						}
						elseif($item->getId() == 269){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		}
	}
	
						}
						elseif($item->getId() == 291){
						foreach($player->getInventory()->getContents() as $item){
						if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0 ){
					
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x +1),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt6 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x -1),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt2 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x  ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z +1) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt3 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z -1) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt4 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () +1 ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt5 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () -1),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball2 = Entity::createEntity ( "Snowball", $player->level, $nbt2, $player );
		$snowball3 = Entity::createEntity ( "Snowball", $player->level, $nbt3, $player );
		$snowball4 = Entity::createEntity ( "Snowball", $player->level, $nbt4, $player );
		$snowball5 = Entity::createEntity ( "Snowball", $player->level, $nbt5, $player );
		$snowball6 = Entity::createEntity ( "Snowball", $player->level, $nbt6, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball2->setMotion ( $snowball2->getMotion ()->multiply ( $f ) );
		$snowball3->setMotion ( $snowball3->getMotion ()->multiply ( $f ) );
		$snowball4->setMotion ( $snowball4->getMotion ()->multiply ( $f ) );
		$snowball5->setMotion ( $snowball5->getMotion ()->multiply ( $f ) );
		$snowball6->setMotion ( $snowball6->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$snowball2->spawnToAll ();
		$snowball3->spawnToAll ();
		$snowball4->spawnToAll ();
		$snowball5->spawnToAll ();
		$snowball6->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		if ($snowball instanceof Projectile) {
			$this->server->getPluginManager ()->callEvent ( $projectileEv = new ProjectileLaunchEvent ( $snowball ) );
			if ($projectileEv->isCancelled ()) {
				$snowball->kill ();
			} else {
				
				$this->object_hash [spl_object_hash ( $snowball )] = 1;
				$snowball->spawnToAll ();
			}
		} else {
			$this->object_hash [spl_object_hash ( $snowball )] = 1;
			$snowball->spawnToAll ();
		}
		}
		}
						}
						elseif($item->getId() == 275){
						foreach($player->getInventory()->getContents() as $item){
						if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0 ){
					
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x +1),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt6 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x -1),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball6 = Entity::createEntity ( "Snowball", $player->level, $nbt6, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball6->setMotion ( $snowball6->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$snowball6->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		if ($snowball instanceof Projectile) {
			$this->server->getPluginManager ()->callEvent ( $projectileEv = new ProjectileLaunchEvent ( $snowball ) );
			if ($projectileEv->isCancelled ()) {
				$snowball->kill ();
			} else {
				
				$this->object_hash [spl_object_hash ( $snowball )] = 1;
				$snowball->spawnToAll ();
			}
		} else {
			$this->object_hash [spl_object_hash ( $snowball )] = 1;
			$snowball->spawnToAll ();
		}
		}
		}
						}
						elseif($item->getId() == 274){
						foreach($player->getInventory()->getContents() as $item){
						if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0 ){
					
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x +1),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt6 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x -1),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt2 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x  ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z +1) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt3 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z -1) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt4 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () +1 ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt5 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () -1),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball2 = Entity::createEntity ( "Snowball", $player->level, $nbt2, $player );
		$snowball3 = Entity::createEntity ( "Snowball", $player->level, $nbt3, $player );
		$snowball4 = Entity::createEntity ( "Snowball", $player->level, $nbt4, $player );
		$snowball5 = Entity::createEntity ( "Snowball", $player->level, $nbt5, $player );
		$snowball6 = Entity::createEntity ( "Snowball", $player->level, $nbt6, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball2->setMotion ( $snowball2->getMotion ()->multiply ( $f ) );
		$snowball3->setMotion ( $snowball3->getMotion ()->multiply ( $f ) );
		$snowball4->setMotion ( $snowball4->getMotion ()->multiply ( $f ) );
		$snowball5->setMotion ( $snowball5->getMotion ()->multiply ( $f ) );
		$snowball6->setMotion ( $snowball6->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$snowball2->spawnToAll ();
		$snowball3->spawnToAll ();
		$snowball4->spawnToAll ();
		$snowball5->spawnToAll ();
		$snowball6->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		if ($snowball instanceof Projectile) {
			$this->server->getPluginManager ()->callEvent ( $projectileEv = new ProjectileLaunchEvent ( $snowball ) );
			if ($projectileEv->isCancelled ()) {
				$snowball->kill ();
			} else {
				
				$this->object_hash [spl_object_hash ( $snowball )] = 1;
				$snowball->spawnToAll ();
			}
		} else {
			$this->object_hash [spl_object_hash ( $snowball )] = 1;
			$snowball->spawnToAll ();
		}
		}
		}
						}
elseif($item->getId() == 273){
						foreach($player->getInventory()->getContents() as $item){
						if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0 ){
					
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x +1),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt6 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x -1),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt2 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x  ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z +1) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt3 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z -1) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt4 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () +1 ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		$nbt5 = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () -1),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		
		$f = 1.5;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball2 = Entity::createEntity ( "Snowball", $player->level, $nbt2, $player );
		$snowball3 = Entity::createEntity ( "Snowball", $player->level, $nbt3, $player );
		$snowball4 = Entity::createEntity ( "Snowball", $player->level, $nbt4, $player );
		$snowball5 = Entity::createEntity ( "Snowball", $player->level, $nbt5, $player );
		$snowball6 = Entity::createEntity ( "Snowball", $player->level, $nbt6, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball2->setMotion ( $snowball2->getMotion ()->multiply ( $f ) );
		$snowball3->setMotion ( $snowball3->getMotion ()->multiply ( $f ) );
		$snowball4->setMotion ( $snowball4->getMotion ()->multiply ( $f ) );
		$snowball5->setMotion ( $snowball5->getMotion ()->multiply ( $f ) );
		$snowball6->setMotion ( $snowball6->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$snowball2->spawnToAll ();
		$snowball3->spawnToAll ();
		$snowball4->spawnToAll ();
		$snowball5->spawnToAll ();
		$snowball6->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		if ($snowball instanceof Projectile) {
			$this->server->getPluginManager ()->callEvent ( $projectileEv = new ProjectileLaunchEvent ( $snowball ) );
			if ($projectileEv->isCancelled ()) {
				$snowball->kill ();
			} else {
				
				$this->object_hash [spl_object_hash ( $snowball )] = 1;
				$snowball->spawnToAll ();
			}
		} else {
			$this->object_hash [spl_object_hash ( $snowball )] = 1;
			$snowball->spawnToAll ();
		}
		}
		}
						}
						elseif($item->getId() == 292){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0 ){
				    $this->getServer()->getScheduler()->scheduleTask(
				        new BurstSnowballTask($this, $player)
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        5
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        10
                    );

					$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
					}
					}
						}
						elseif($item->getId() == 256){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0 ){
                    $this->getServer()->getScheduler()->scheduleTask(
                        new BurstSnowballTask($this, $player)
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        5
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        10
                    );
					$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
					}
					}
						}
						elseif($item->getId() == 257){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0 ){
                    $this->getServer()->getScheduler()->scheduleTask(
                        new BurstSnowballTask($this, $player)
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        5
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        10
                    );

					$player->getInventory ()->removeItem ( Item::get(351, 8, 1) );
					}
					}
						}
						elseif($item->getId() == 258){
		foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0 ){
                    $this->getServer()->getScheduler()->scheduleTask(
                        new BurstSnowballTask($this, $player)
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        5
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        10
                    );

					$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
					}
					}
						}elseif($item->getId() == 293){
						foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
                    $this->getServer()->getScheduler()->scheduleTask(
                        new BurstSnowballTask($this, $player)
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        7
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        14
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        21
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        28
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        35
                    );

					$player->getInventory()->removeItem (Item::get(289, 0, 1));
					}
					}
						}
						elseif($item->getId() == 284){
						foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
                    $this->getServer()->getScheduler()->scheduleTask(
                        new BurstSnowballTask($this, $player)
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        7
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        14
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        21
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        28
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        35
                    );
					$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
					}
					}
						}
						elseif($item->getId() == 285){
						foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
                    $this->getServer()->getScheduler()->scheduleTask(
                        new BurstSnowballTask($this, $player)
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        7
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        14
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        21
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        28
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        35
                    );
					$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
					}
					}
						}
						elseif($item->getId() == 286){
						foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
                    $this->getServer()->getScheduler()->scheduleTask(
                        new BurstSnowballTask($this, $player)
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        7
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        14
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        21
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        28
                    );

                    $this->getServer()->getScheduler()->scheduleDelayedTask(
                        new BurstSnowballTask($this, $player),
                        35
                    );
					$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
					}
					}
						}elseif($item->getId() == 294){
						foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		
		$f = 3.0;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		}
		}
		
						}
						elseif($item->getId() == 277){
						foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		
		$f = 3.0;
		$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		}
		}
		
						}
						elseif($item->getId() == 278){
						foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 289 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		
		$f = 3.0;
		$snowball = Entity::createEntity ( "PrimedTNT", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(289, 0, 1) );
		}
		}
		   }
		elseif($item->getId() == 46){
						foreach($player->getInventory()->getContents() as $item){
				if ($item->getID() == 46 && $item->getDamage() == 0 && $item->getCount() > 0){
						$nbt = new CompoundTag ( "", [ 
				"Pos" => new ListTag ( "Pos", [ 
						new DoubleTag ( "", $player->x ),
						new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
						new DoubleTag ( "", $player->z ) 
				] ),
				"Motion" => new ListTag ( "Motion", [ 
						new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
						new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) 
				] ),
				"Rotation" => new ListTag ( "Rotation", [ 
						new FloatTag ( "", $player->yaw ),
						new FloatTag ( "", $player->pitch ) 
				] ) 
		] );
		
		
		$f = 3.0;
		$snowball = Entity::createEntity ( "PrimedTNT", $player->level, $nbt, $player );
		$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		$snowball->spawnToAll ();
		$player->getInventory ()->removeItem ( Item::get(46, 0, 1) );
		}
		}
						}else{
			}						
		}
	// public function burstSnowball(Player $player) {
	// 	$nbt = new CompoundTag ( "", [ 
	// 			"Pos" => new ListTag ( "Pos", [ 
	// 					new DoubleTag ( "", $player->x ),
	// 					new DoubleTag ( "", $player->y + $player->getEyeHeight () ),
	// 					new DoubleTag ( "", $player->z ) ] ),
	// 			"Motion" => new ListTag ( "Motion", [ 
	// 					new DoubleTag ( "", - \sin ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ),
	// 					new DoubleTag ( "", - \sin ( $player->pitch / 180 * M_PI ) ),
	// 					new DoubleTag ( "",\cos ( $player->yaw / 180 * M_PI ) *\cos ( $player->pitch / 180 * M_PI ) ) ] ),
	// 			"Rotation" => new ListTag ( "Rotation", [ 
	// 					new FloatTag ( "", $player->yaw ),
	// 					new FloatTag ( "", $player->pitch ) ] ) ] );
		
	// 	$f = 1.5;
	// 	$snowball = Entity::createEntity ( "Snowball", $player->level, $nbt, $player );
	// 	$snowball->setMotion ( $snowball->getMotion ()->multiply ( $f ) );
		
	// 	if ($snowball instanceof Projectile) {
	// 		$this->server->getPluginManager ()->callEvent ( $projectileEv = new ProjectileLaunchEvent ( $snowball ) );
	// 		if ($projectileEv->isCancelled ()) {
	// 			$snowball->kill ();
	// 		} else {
				
	// 			$this->object_hash [spl_object_hash ( $snowball )] = 1;
	// 			$snowball->spawnToAll ();
	// 		}
	// 	} else {
	// 		$this->object_hash [spl_object_hash ( $snowball )] = 1;
	// 		$snowball->spawnToAll ();
	// 	}
	// }
		public function onDamage(EntityDamageEvent $event){
		$player = $event->getEntity();
		$entity = $event->getEntity();
        if($player instanceof Player && $event->getCause() === EntityDamageEvent::CAUSE_PROJECTILE){
                $event->setDamage(6);
        }
	}
}
?>