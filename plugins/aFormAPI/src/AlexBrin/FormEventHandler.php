<?php

namespace AlexBrin;

use AlexBrin\elements\forms\Form;
use pocketmine\event\Listener;
use pocketmine\event\server\DataPacketReceiveEvent;
use pocketmine\network\mcpe\protocol\ModalFormResponsePacket;

class FormEventHandler implements Listener {

    /**
     * @var aFormAPI
     */
    private $plugin;

    public function __construct(aFormAPI $plugin) {
        $this->plugin = $plugin;
    }

    public function onPacketReceive(DataPacketReceiveEvent $event) {
        $packet = $event->getPacket();
        if($packet instanceof ModalFormResponsePacket) {
            $player = $event->getPlayer();
            $formId = $packet->formId;
            $formData = json_decode($packet->formData, true);
            if(isset($this->plugin->forms[$formId])) {
                /* @var Form $form */
                $form = $this->plugin->forms[$formId];
                if(!$form->isRecipient($player))
                    return;

                $callable = $form->getCallable();
                if(!is_array($formData))
                    $formData = [$formData];

                if($callable !== null)
                    $callable($event->getPlayer(), $formData);

                unset($this->plugin->forms[$formId]);
                $event->setCancelled(true);
            }
        }
    }

}