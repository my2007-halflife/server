<?php

namespace AlexBrin;

use AlexBrin\elements\Button;
use AlexBrin\elements\forms\Form;
use AlexBrin\elements\Label;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\Player;

class TestCommand extends Command {

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        if(!$sender instanceof Player)
            return true;

        $form = new Form(100500, function($player, $data) {
            print_r($data);
        });
        $form->data['type'] = 'custom_form';
        $form->data['size'] = [500, 5000];

//        $form->addElement(new Label('Текст'));

        $form->sendForm($sender);

        return true;
    }
}