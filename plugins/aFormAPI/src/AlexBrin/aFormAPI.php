<?php

namespace AlexBrin;

use AlexBrin\elements\forms\CustomForm;
use AlexBrin\elements\forms\ModalWindow;
use AlexBrin\elements\forms\SimpleForm;
use pocketmine\event\Listener;
use pocketmine\plugin\PluginBase;

class aFormAPI extends PluginBase implements Listener {

    public $formsCount = 0;

    public $forms = [];

    private static $instance;

    public function onEnable() {
        $this->getServer()->getPluginManager()->registerEvents(new FormEventHandler($this), $this);

//        $this->getServer()->getCommandMap()->register(
//            't',
//            new TestCommand('t', 'Test command')
//        );

        self::$instance = &$this;
    }

    public function createSimpleForm(callable $callable = null): SimpleForm {
        $this->formsCount++;

        $form = new SimpleForm($this->formsCount, $callable);
        if($callable !== null)
            $this->forms[$this->formsCount] = $form;

        return $form;
    }

    public function createModalForm($content, $trueButton, $falseButton, callable $callable = null): ModalWindow {
        $this->formsCount++;

        $form = new ModalWindow($this->formsCount, $content, $trueButton, $falseButton, $callable);
        if($callable !== null)
            $this->forms[$this->formsCount] = $form;

        return $form;
    }

    public function createCustomForm(callable $callable = null) {
        $this->formsCount++;

        $form = new CustomForm($this->formsCount, $callable);
        if($callable !== null)
            $this->forms[$this->formsCount] = $form;

        return $form;
    }

    public static function getInstance(): APICore {
        return self::$instance;
    }

}