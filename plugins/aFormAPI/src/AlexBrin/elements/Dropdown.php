<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.10.2017
 * Time: 2:11
 */

namespace AlexBrin\elements;

use pocketmine\Player;

class Dropdown extends Element implements \JsonSerializable {

    protected $options = [];
    protected  $defaultOptionIndex = 0;

    public function __construct($text, $options = []) {
        $this->text = $text;
        $this->options = $options;
    }

    public function addOption($optionText, $isDefault = false): Dropdown {
        if($isDefault)
            $this->defaultOptionIndex = count($this->options);

        $this->options[] = $optionText;

        return $this;
    }

    public function setOptionAsDefault($optionText): Dropdown {
        $index = array_search($optionText, $this->options);
        if($index === false)
            return $this;

        $this->defaultOptionIndex = $index;
        return $this;
    }

    public function setOptions(array $options = []) {
        $this->options = $options;
        return $this;
    }

    public function jsonSerialize() {
        return [];
    }

    public function handle($value, Player $player) {
        return $this->options[$value];
    }
}