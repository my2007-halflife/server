<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.10.2017
 * Time: 1:57
 */

namespace AlexBrin\elements;


use pocketmine\Player;

class Image extends Element {

    protected $texture;
    protected $width;
    protected $height;

    public function __construct($texture, $width = 0, $height = 0) {
        $this->texture = $texture;
        $this->width = 0;
        $this->height = 0;
    }

    final public function jsonSerialize() {
        return [
            'text' => 'sign',
            'type' => 'image',
            'texture' => $this->texture,
            'size' => [
                $this->width,
                $this->height
            ]
        ];
    }

    public function handle($value, Player $player) {
        return null;
    }
}