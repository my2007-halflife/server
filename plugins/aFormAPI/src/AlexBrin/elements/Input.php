<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.10.2017
 * Time: 2:16
 */

namespace AlexBrin\elements;

use pocketmine\Player;

class Input extends Element implements \JsonSerializable {

    protected $placeholder;
    protected $defaultText;

    public function __construct($text, $placeholder = '', $defaultText = '') {
        $this->text = $text;
        $this->placeholder = $placeholder;
        $this->defaultText = $defaultText;
    }

    function jsonSerialize() {
        return [
            'type' => 'input',
            'text' => $this->text,
            'placeholder' => $this->placeholder,
            'default' => $this->defaultText,
        ];
    }

    public function handle($value, Player $player) {
        return $value;
    }
}