<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.10.2017
 * Time: 2:15
 */

namespace AlexBrin\elements;

use pocketmine\Player;

class Label extends Element implements \JsonSerializable {

    public function __construct($text) {
        $this->text = $text;
    }

    final public function jsonSerialize() {
        return [
            'type' => 'label',
            'text' => $this->text,
        ];
    }

    public function handle($value, Player $player) {
        return $this->text;
    }
}