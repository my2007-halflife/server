<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.10.2017
 * Time: 2:01
 */

namespace AlexBrin\elements;


use pocketmine\Player;

class StepSlider extends Element implements \JsonSerializable {

    protected $steps = [];
    protected $defaultStepIndex = 0;

    public function __construct($text, $steps = []) {
        $this->text = $text;
        $this->steps = $steps;
    }

    public function addStep($stepText, $isDefault = false): StepSlider {
        if($isDefault)
            $this->defaultStepIndex = count($this->steps);

        $this->steps[] = $stepText;

        return $this;
    }

    public function setStepAsDefault($stepText): StepSlider {
        $index = array_search($stepText, $this->steps);
        if($index !== null)
            $this->defaultStepIndex = $index;
        return $this;
    }

    public function setSteps(array $steps = []): StepSlider {
        $this->steps = $steps;

        return $this;
    }

    final public function jsonSerialize() {
        return [
            'type' => 'step_slider',
            'text' => $this->text,
            'steps' => array_map('strval', $this->steps),
            'default' => $this->defaultStepIndex
        ];
    }

    public function handle($value, Player $player) {
        return $this->steps[$value];
    }
}