<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.10.2017
 * Time: 2:21
 */

namespace AlexBrin\elements;


use pocketmine\Player;

class Toggle extends Element implements \JsonSerializable {

    protected $defaultValue = false;

    public function __construct($text, bool $value = false){
        $this->text = $text;
        $this->defaultValue = $value;
    }

    public function setDefaultValue(bool $value){
        $this->defaultValue = $value;
        return $this;
    }

    public function jsonSerialize(){
        return [
            "type" => "toggle",
            "text" => $this->text,
            "default" => $this->defaultValue
        ];
    }

    public function handle($value, Player $player){
        return $value;
    }

}