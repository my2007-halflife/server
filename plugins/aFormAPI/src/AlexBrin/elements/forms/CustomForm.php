<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.10.2017
 * Time: 1:21
 */

namespace AlexBrin\elements\forms;


use AlexBrin\elements\Element;

class CustomForm extends Form implements \JsonSerializable {

    public function __construct($id, callable $callable) {
        parent::__construct($id, $callable);

        $this->data['type'] = 'custom_form';
        $this->data['content'] = [];
    }

    public function addElement(Element $element): CustomForm {
        $this->data['content'][] = $element;
        return $this;
    }

    public function addIconUrl(string $url): CustomForm {
        $this->data['icon'] = [
            'type' => 'url',
            'data' => $url,
        ];
        return $this;
    }

    public function jsonSerialize() {
        return $this->data;
    }

}