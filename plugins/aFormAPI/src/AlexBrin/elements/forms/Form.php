<?php

namespace AlexBrin\elements\forms;

use pocketmine\network\mcpe\protocol\ModalFormRequestPacket;
use pocketmine\Player;

class Form implements \JsonSerializable {

    /* @var int $id */
    public $id;
    /* @var array $data */
    public $data = [];
    /* @var string $playerName */
    public $playerName;
    /* @var callable $callable */
    public $callable;

    public function __construct(int $id, callable $callable = null) {
        $this->id = $id;
        $this->callable = $callable;

        $this->data['title'] = '';
        $this->data['content'] = '';
    }

    public function getId(): int {
        return $this->id;
    }

    public function getData(): array {
        return $this->data;
    }

    public function getCallable(): callable {
        return $this->callable;
    }

    public function isRecipient(Player $player): bool {
        return $player->getName() === $this->playerName;
    }

    public function setTitle(string $title) {
        $this->data['title'] = $title;
        return $this;
    }

    public function getTitle(): string {
        return $this->data['title'];
    }

    public function setContent(string $content) {
        $this->data['content'] = $content;
    }

    public function getContent(): string {
        return $this->data['content'];
    }

    public function sendForm(Player $player) {
        $this->playerName = $player->getName();

        $packet = new ModalFormRequestPacket();
        $packet->formId = $this->id;
        $packet->formData = json_encode($this->data);
        $player->dataPacket($packet);
    }

    public function jsonSerialize() {
        return [];
    }

}