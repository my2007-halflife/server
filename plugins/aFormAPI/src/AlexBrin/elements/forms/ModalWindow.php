<?php

namespace AlexBrin\elements\forms;


class ModalWindow extends Form implements \JsonSerializable {

    public function __construct($id, $content, $trueButton, $falseButton, callable $callable = null) {
        parent::__construct($id, $callable);

        $this->data['type'] = 'modal';
        $this->data['content'] = $content;
        $this->data['button1'] = $trueButton;
        $this->data['button2'] = $falseButton;
    }

    public function jsonSerialize() {
        return $this->data;
    }

}