<?php

namespace AlexBrin\elements\forms;

use AlexBrin\elements\Button;

class SimpleForm extends Form implements \JsonSerializable {

    public function __construct($id, callable $callable = null) {
        parent::__construct($id, $callable);

        $this->data['type'] = 'form';
        $this->data['buttons'] = [];
    }

    public function getId(): int {
        return $this->id;
    }

    public function addButton(Button $button) {
        $this->data['buttons'][] = $button;
        return $this;
    }

    public function jsonSerialize() {
        return $this->data;
    }

}