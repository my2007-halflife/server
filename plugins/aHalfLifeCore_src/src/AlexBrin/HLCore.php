<?php

/**
 * Ядро
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin;

use _64FF00\PurePerms\PurePerms;
use AlexBrin\commands\CreateNpcCommand;
use AlexBrin\commands\ExpCommand;
use AlexBrin\commands\GroupSpawnCommand;
use AlexBrin\commands\MyPosCommand;
use AlexBrin\commands\PardonCommand;
use AlexBrin\sound\TeleportEndermanSound;
use AlexBrin\utils\EconomyManager;
use AlexBrin\utils\Group;
use AlexBrin\commands\BanCommand;
use pocketmine\entity\Entity;
use pocketmine\entity\Human;
use pocketmine\event\Event;
use pocketmine\event\Listener;
use pocketmine\inventory\Inventory;
use pocketmine\item\Item;
use pocketmine\level\Location;
use pocketmine\math\Vector3;
use pocketmine\nbt\tag\CompoundTag;
use pocketmine\nbt\tag\DoubleTag;
use pocketmine\nbt\tag\FloatTag;
use pocketmine\nbt\tag\ListTag;
use pocketmine\nbt\tag\StringTag;
use pocketmine\Player;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\Config;

class HLCore extends PluginBase implements Listener {

    /* @var Config $config */
    /* @var Config $messages */
    /* @var Config $players */
    /* @var Config $groups */
    /* @var Config $npc */
    /* @var Config $items */
    /* @var Config $commands */
    public $config,
           $messages,
           $players,
           $groups,
           $bans,
           $npc,
           $items,
           $commands;

    public $eco;

    public $hotbar = [];

    /* @var aFormAPI $FormAPI */
    private static $FormAPI;

    /**
     * @var PurePerms $pureperms
     */
    private $pureperms;

    private static $instance;

    public function onEnable() {
        $f = $this->getDataFolder();
        if(!is_dir($f))
            @mkdir($f);

        $this->getServer()->getLogger()->notice('[HalfLife] Загрузка ядра');

        $this->saveResource('config.yml');
        $this->saveResource('messages.yml');
        $this->saveResource('groups.yml');
        $this->saveResource('items.yml');
        $this->saveResource('commands.yml');

        $this->config = new Config($f.'config.yml', Config::YAML);
        $this->messages = new Config($f.'messages.yml', Config::YAML);
        $this->players = new Config($f.'players.json', Config::JSON);
        $this->groups = new Config($f.'groups.yml', Config::YAML);
        $this->bans = new Config($f.'bans.json', Config::JSON);
        $this->npc = new Config($f.'npc.yml', Config::YAML);
        $this->items = new Config($f.'items.yml', Config::YAML);
        $this->commands = new Config($f.'commands.yml', Config::YAML);

        $this->eco = new EconomyManager($this);

        $this->getServer()->getPluginManager()->registerEvents(new HLEventListener($this), $this);

        $this->getServer()->getScheduler()->scheduleRepeatingTask(
            new PayDay($this),
            $this->getParam('payday.period', 300) * 20
        );

        $this->getServer()->getScheduler()->scheduleRepeatingTask(
            new Hotbar($this),
            20
        );

        $this->registerCommands();

        self::$FormAPI = $this->getServer()->getPluginManager()->getPlugin('aFormAPI');
        $this->pureperms = $this->getServer()->getPluginManager()->getPlugin('PurePerms');

        self::$instance = &$this;

        $this->getServer()->getLogger()->notice("[HalfLife] Готов к работе");
    }

    /**
     * @param Player $player
     */
    public function createItems(Player $player) {
        $inv = $player->getInventory();

        foreach($this->getParam('items', []) as $itemId) {
            if(!$this->isItemExist($inv, $itemId)) {
                $this->addItem($inv, $itemId);
            }
        }

        $groupItems = $this->items->get(Group::getGroupByPlayer($player)->code);
        if(is_array($groupItems))
            foreach($groupItems as $itemId)
                if(!$this->isItemExist($inv, $itemId))
                    $this->addItem($inv, $itemId);
    }

    /**
     * @param Inventory $inv
     * @param Item|int $item
     */
    public function addItem(&$inv, $item) {
        if(!$item instanceof Item)
            $item = Item::get($item);

        $inv->addItem($item);
    }

    /**
     * @param Inventory $inv
     * @param int $itemId
     * @return bool
     */
    public function isItemExist(Inventory &$inv, int $itemId): bool {
        foreach($inv->getContents() as $item)
            if($item->getId() == $itemId)
                return true;
        return false;
    }

    /**
     * Регистрация команд
     */
    private function registerCommands() {
        $this->getServer()->getLogger()->notice('[HalfLife] Регистрация команд...');

        $commandMap = $this->getServer()->getCommandMap();

        $cmd = $commandMap->getCommand('ban');
        $cmd->unregister($commandMap);
        $cmd->setLabel('');

        $cmd = $commandMap->getCommand('pardon');
        $cmd->unregister($commandMap);
        $cmd->setLabel('');

        $commandMap->register(
            'ban',
            new BanCommand(
                'ban',
                'Бан игроков',
                'halflife.command.ban',
                '§eИспользование: /ban <ник> <EXP (payday)> [причина]'
            )
        );

        $commandMap->register(
            'pardon',
            new PardonCommand(
                'pardon',
                'Разбан игроков',
                'halflife.command.pardon',
                '§eИспользование: /ban <ник>',
                ['unban']
            )
        );

        $commandMap->register(
            'cnpc',
            new CreateNpcCommand(
                'cnpc',
                'Создание NPC фракций',
                'halflife.command.cnpc',
                '§eИспользование: /cnpc <имя NPC>',
                ['createnpc', 'npc']
            )
        );

        $commandMap->register(
            'exp',
            new ExpCommand(
                'exp',
                'Работа с опытом',
                'halflife.command.exp'
            )
        );

        $commandMap->register(
            'groupspawn',
            new GroupSpawnCommand(
                'groupspawn',
                'Установка точки спавна для группы',
                'halflife.command.groupspawn',
                '§eИспользование: /groupspawn <код группы>',
                ['gspawn']
            )
        );

        $commandMap->register(
            'mypos',
            new MyPosCommand(
                'mypos',
                'Позиция игрока',
                'halflife.command.mypos'
            )
        );
    }

    /**
     * Отправляет сообщение в радиусе, указанном в конфиге
     *
     * @param String $message
     * @param String|null $info
     * @param Player $player
     */
    public function sendMessageWithDistance(String $message, String $info = null, Player $player) {
        foreach($player->level->getPlayers() as $pl)
            if($player->distance($pl->getPosition()) <= $this->getParam('params.showDistance', 20)) {
                $pl->sendMessage(str_replace('\n', "\n", $message));
                if($info)
                    $pl->sendMessage($info);
            }

        $player->level->addSound(new TeleportEndermanSound($player->getPosition()));
    }

    public static function getFormApi(): aFormAPI {
        return self::$FormAPI;
    }

    public function getConfig(): Config {
        return $this->config;
    }

    public function getMessages(): Config {
        return $this->messages;
    }

    /**
     * @param  Player|String $player
     * @return array
     */
    public function getGroupsByPlayer($player): array {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        return $this->players->getNested($player.'.groups', []);
    }

    /**
     * @param Group|string $group
     * @return bool
     */
    public function isGroupLimit($group): bool {
        if(!$group instanceof Group)
            $group = new Group($group);

        $limit = $group->limit;

        if($limit == '*')
            return false;

        $count = count(Group::getOnlinePlayers($group));

        return $count >= $limit;
    }

    public function getGroupByPlayer($player): Group {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        $playerInfo = $this->getPlayerInfo($player);

        return new Group($playerInfo["group"]);
    }

    public function getPlayerInfo($player): array {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        return $this->players->get($player);
    }

    /**
     * @param string $message
     * @param Player[]|null $players
     */
    public static function sendBroadcast(string $message, $players = null) {
        self::getInstance()->getServer()->broadcastMessage($message, $players);
    }

    /**
     * @param $node
     * @param array $vars
     * @return string
     */
    public function getMessage($node, $vars = []): string {
        $message = $this->messages->getNested($node, '');

        $i = 0;
        foreach($vars as $var) {
            $message = str_replace("%var$i%", $var, $message);
            $i++;
        }

        return $message;
    }

    public function createNPC(Location $pos, $skin, $skinId, $name): Human {
        $npc = new Human($pos->level,
            new CompoundTag("", [
                "Pos" => new ListTag("Pos", [
                    new DoubleTag("", $pos->x),
                    new DoubleTag("", $pos->y),
                    new DoubleTag("", $pos->z),
                ]),
                "Motion" => new ListTag("Motion", [
                    new DoubleTag("", 0),
                    new DoubleTag("", 0),
                    new DoubleTag("", 0),
                ]),
                "Rotation" => new ListTag("Rotation", [
                    new FloatTag("", $pos->yaw),
                    new FloatTag("", $pos->pitch),
                ]),
                "Skin" => new CompoundTag("Skin", [
                    "Data" => new StringTag("Data", $skin),
                    "Name" => new StringTag("Name", $skinId),
                ]),
            ])
        );

        $npc->setDataProperty(Entity::DATA_NAMETAG, Entity::DATA_TYPE_STRING, $name);
        if($this->getParam('npc.nametagVisible', true)) {
            $npc->setDataFlag(Entity::DATA_FLAGS, Entity::DATA_FLAG_CAN_SHOW_NAMETAG, true);
            $npc->setDataFlag(Entity::DATA_FLAGS, Entity::DATA_FLAG_ALWAYS_SHOW_NAMETAG, true);
        }
        $npc->spawnToAll();

        return $npc;
    }

    public function addGroupByPlayer($player, $group): bool {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        if($group instanceof Group)
            $group = $group->code;

        $groups = $this->players->getNested("$player.groups");
        if(!in_array($group, $groups)) {
            $groups[] = $group;
            $this->players->setNested("$player.groups", $groups);
            $this->players->save(true);
            return true;
        }

        return false;
    }

    public function setGroup($player, $group) {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        if(!$group instanceof Group)
            $group = new Group($group);

        if($this->isGroupLimit($group))
            return;

        $this->players->setNested($player.'.group', $group->code);
        $this->players->save(true);

        Group::changeGroup($player, $group->code);
    }

    public function chatFormat(Player $player, $message): string {
        $text = $this->getParam('params.chatFormat', '');

        $group = Group::getGroupByPlayer($player);

        $text = str_replace('{group}', $group->getName(), $text);
        $text = str_replace('{nickname}', $player->getName(), $text);
        $text = str_replace('{message}', $message, $text);

        return $text;
    }

    public function getParam($node, $default = null) {
        return $this->getConfig()->getNested($node, $default);
    }

    public static function getSkinByHp(float $hp, Group $group): int {
        $hp = (int) $hp;
        if($hp < 20 && !isset($group->hp[$hp]))
            while(!isset($group->hp[$hp]))
                $hp++;
        elseif($hp >= 20)
            $hp = 20;

        return $hp;
    }

    public static function sUpdateHotbar($player, $format) {
        HLCore::getInstance()->updateHotbar($player, $format);
    }

    public function updateHotbar($player, $format) {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        $this->hotbar[$player] = $format;
    }

    public function getPurePerms(): PurePerms {
        return $this->pureperms;
    }

    /**
     * @param Group|string $group
     * @param Vector3|float[] $pos
     */
    public function setGroupSpawn($group, $pos) {
        if($pos instanceof Vector3)
            $pos = ['x' => $pos->x, 'y' => $pos->y, 'z' => $pos->z];

        $this->groups->setNested($group.'.spawn', [$pos]);
        $this->groups->save();
    }

    public static function callEvent(Event $event) {
        self::getInstance()->getServer()->getPluginManager()->callEvent($event);
    }

    public static function getInstance(): HLCore {
        return self::$instance;
    }

}