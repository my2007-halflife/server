<?php
/**
 * Обработчик событий
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

declare(strict_types=1);

namespace AlexBrin;

use _64FF00\PureChat\PureChat;
use AlexBrin\elements\Button;
use AlexBrin\events\PlayerShowCIDCardEvent;
use AlexBrin\sound\TeleportEndermanSound;
use AlexBrin\utils\Ban;
use AlexBrin\utils\EconomyManager;
use AlexBrin\utils\Exp;
use AlexBrin\utils\Group;
use pocketmine\entity\Human;
use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\event\entity\EntityDamageEvent;
use pocketmine\event\entity\EntityExplodeEvent;
use pocketmine\event\inventory\InventoryPickupItemEvent;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerChatEvent;
use pocketmine\event\player\PlayerCommandPreprocessEvent;
use pocketmine\event\player\PlayerDeathEvent;
use pocketmine\event\entity\EntityRegainHealthEvent;
use pocketmine\event\player\PlayerDropItemEvent;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\event\player\PlayerJoinEvent;
use pocketmine\event\player\PlayerPreLoginEvent;
use pocketmine\event\player\PlayerQuitEvent;
use pocketmine\event\player\PlayerRespawnEvent;
use pocketmine\inventory\Inventory;
use pocketmine\item\Item;
use pocketmine\Player;
use pocketmine\Server;

class HLEventListener implements Listener {

    private $showCards = [];

    private $selectGroup = [];

    private $selectMessage = [];

    private $selectCommand = [];

    private $death = [];

    /* @var HLCore $plugin */
    private $plugin;

    /* @var PureChat $pChat */
    private $pChat;

    public $dialog = [];

    private static $instance;

    public function __construct(HLCore &$plugin) {
        $this->plugin = $plugin;
        $this->pChat = $plugin->getServer()->getPluginManager()->getPlugin('PureChat');

        self::$instance = &$this;
    }

    public function onPlayerPreLogin(PlayerPreLoginEvent $event) {
        $nickname = mb_strtolower($event->getPlayer()->getName());

        if(Ban::isPlayerBanned($nickname))
            $this->getPlugin()->hotbar[$nickname] = $this->getParam('hotbar.ban');
        else
            $this->getPlugin()->hotbar[$nickname] = $this->getParam('hotbar.player');
    }

    public function onPlayerJoin(PlayerJoinEvent $event) {
        $player = $event->getPlayer();
        $player->setHealth(20);

        $nickname = mb_strtolower($player->getName());

        if(Ban::isPlayerBanned($player)) {
            $group = $this->getParam('defaultValues.banGroup');
            $this->getPlugin()->players->setNested($nickname.'.group', $group);
            $this->getPlugin()->players->save();
            Group::changeGroup($player, $group);
            $player->setHealth(20);
            aSkinSaver::getInstance()->setSkin($player, (new Group($group))->hp[20]);
            return;
        }

        $this->createItems($player);
        $playerInfo = $this->getPlugin()->players->get($nickname);
        if(!$playerInfo) {
            $defaultGroup = $this->getParam('defaultValues.group');
            $this->getPlugin()->players->set($nickname, [
                'group' => $defaultGroup,
                'groups' => [$defaultGroup],
                'exp' => 0,
            ]);
        } else {
            $defaultGroup = $this->getParam('defaultValues.secondGroup');
            if(Group::isGroupExists($player, $defaultGroup))
                $this->getPlugin()->players->setNested($nickname.'.group', $defaultGroup);
            else {
                $defaultGroup = $this->getParam('defaultValues.group');
                $this->getPlugin()->players->setNested($nickname.'.group', $defaultGroup);
            }
        }
        $this->getPlugin()->players->save();

        Group::changeGroup($player, $defaultGroup);

        $group = new Group($defaultGroup);
        $player->teleport($group->getSpawn() ?? $player->level->getSpawnLocation());
        $player->setHealth(20);
        aSkinSaver::getInstance()->setSkin($player, $group->hp[20]);
    }

    public function onPlayerQuit(PlayerQuitEvent $event) {
        $nickname = $event->getPlayer()->getName();
        if(isset($this->death[$nickname]))
            unset($this->death[$nickname]);
        unset($this->getPlugin()->hotbar[mb_strtolower($nickname)]);
    }

    /**
     * Изменяет скин при уроне
     * Открывает форму выбора профессии при клике на NPC
     * Отменяет урон NPC от взрыва
     *
     * @param EntityDamageEvent $event
     */
    public function onEntityDamage(EntityDamageEvent $event) {
        $entity = $event->getEntity();
        $npc = $this->getPlugin()->npc->get(str_replace(' ', '_', $entity->getNameTag()));

        if($npc && $event->getCause() == EntityDamageEvent::CAUSE_ENTITY_EXPLOSION) {
            $event->setCancelled(true);
            $entity->teleport($entity->getPosition());
            return;
        }

        if($entity instanceof Player) {
            $hp = (int) $entity->getHealth() - $event->getDamage();
            if($hp < 1)
                return;

            $group = Group::getGroupByPlayer($entity);

            $hp = HLCore::getSkinByHp($hp, $group);

            aSkinSaver::getInstance()->setSkin($entity, $group->hp[$hp]);
        }

        if($event instanceof EntityDamageByEntityEvent) {
            $entity = $event->getEntity();
            $damager = $event->getDamager();

            if(!$damager instanceof Player || !$entity instanceof Human)
                return;

            if($entity instanceof Player && $damager->getGamemode() == Player::CREATIVE) {
                $event->setCancelled(true);
                return;
            }

            if(Ban::isPlayerBanned($damager)) {
                $event->setCancelled(true);
                return;
            }

            if($npc) {
                $event->setCancelled(true);
                $this->createFormSelectGroup($damager, $npc);
            }

            if(Ban::isPlayerBanned($damager)) {
                $event->setCancelled(true);
                return;
            }
        }
    }

    /**
     * Изменяет скин при регенерации
     *
     * @param EntityRegainHealthEvent $event
     */
    public function onEntityRegen(EntityRegainHealthEvent $event) {
        $entity = $event->getEntity();

        if(!$entity instanceof Player)
            return;

        $group = Group::getGroupByPlayer($entity);
        $hp = HLCore::getSkinByHp(
            $entity->getHealth() + $event->getAmount(),
            $group
        );

        aSkinSaver::getInstance()->setSkin($entity, $group->hp[$hp]);
    }

    /**
     * Если первый символ "@" - чат фракции
     * Если первый символ "!" - глобальный чат
     * Иначе чат на расстояние блоков из конфига
     *
     * @param PlayerChatEvent $event
     */
    public function onPlayerChat(PlayerChatEvent $event) {
        $player = $event->getPlayer();

        $levelName = $this->pChat->getConfig()->get("enable-multiworld-chat") ? $player->getLevel()->getName() : null;
        $chatFormat = $this->pChat->getChatFormat($player, $event->getMessage(), $levelName);
        $event->setFormat($chatFormat);

        if(Ban::isPlayerBanned($player)) {
            $event->setCancelled(true);
            return;
        }

        $firstSymbol = $event->getMessage(){0};
        if($firstSymbol == '@') {
            $event->setCancelled(true);

            $group = Group::getGroupByPlayer($player);
            if(!$group->isAccessToFracChat()) {
                $player->sendMessage($this->getMessage('group.fractionalChatDenied'));
                return;
            }
            $group = explode('.', $group->code);

            $prefix = $this->getParam('channel.fraction.' . explode('.', $group->code)[0]);
            $player->sendMessage($prefix.$event->getFormat());

            foreach(Server::getInstance()->getOnlinePlayers() as $pl) {
                $targetGroup = Group::getGroupByPlayer($pl);
                var_dump(explode('.', $targetGroup->code)[0]);
                if($group === explode('.', $targetGroup->code)[0])
                    $pl->sendMessage($prefix.$event->getFormat());
            }

            return;
        }

        if($firstSymbol == '!') {
            $event->setCancelled(true);
            $event = new PlayerChatEvent($player, $event->getMessage());
            $prefix = $this->getParam('channel.global');
            $event->setFormat($prefix . $event->getFormat());
//            $event->setFormat($prefix.$event->getFormat());
//            $event->
            $player->level->addSound(new TeleportEndermanSound($player->getPosition()));
            return;
        }

        $event->setCancelled(true);
        $this->plugin->sendMessageWithDistance($event->getFormat(), null, $player);
    }

    public function onPlayerRespawn(PlayerRespawnEvent $event) {
        $player = $event->getPlayer();
        $group = Group::getGroupByPlayer($player);
        $player->setHealth(20);
        if(isset($group->hp[20]))
            aSkinSaver::getInstance()->setSkin($player, $group->hp[20]);

        if($this->death[$player->getName()])
            $this->createItems($player);

        // Взято из onPlayerDeath
        // Телепорт есть при входе (закомментирован)
        $player->teleport($group->getSpawn() ?? $player->level->getSpawnLocation());

        $this->createItems($event->getPlayer());
    }

    public function onPlayerItemDrop(PlayerDropItemEvent $event) {
        if(Ban::isPlayerBanned($event->getPlayer())) {
            $event->setCancelled(true);
            return;
        }

        $item = $event->getItem()->getId();

        foreach($this->getParam('items') as $itemId)
            if($item == $itemId) {
                $event->setCancelled(true);
                break;
            }
    }

    public function onPlayerItemPickup(InventoryPickupItemEvent $event) {
        $holder = $event->getInventory()->getHolder();
        if($holder instanceof Player) {
            if(Ban::isPlayerBanned($holder))
                $event->setCancelled(true);
        }
    }

    public function onPlayerInteract(PlayerInteractEvent $event) {
        $player = $event->getPlayer();

        if(Ban::isPlayerBanned($player)) {
            $event->setCancelled(true);
            return;
        }

        $nickname = mb_strtolower($player->getName());
        $item = $event->getItem();

        switch($item->getId()) {

            case Item::CAKE_BLOCK:
                    if(!$player->isOp())
                        $event->setCancelled(true);
                break;

            case $this->getParam('items.CIDcard', 265):
                    $group = Group::getGroupByPlayer($player);
                    if($group)
                        $group = $group->getName();

                    if(in_array($group, $this->getParam('params.deniedToCidCard', [])))
                        return;

                    $time = time();
                    if(!isset($this->showCards[$nickname]))
                        $this->showCards[$nickname] = $time + 5;
                    elseif($this->showCards[$nickname] > $time)
                        return;

                    $this->showCards[$nickname] = $time + 5;

                    $ev = new PlayerShowCIDCardEvent($player, $this->getPlugin()->getGroupByPlayer($player->getName()));
                    if($ev->isCancelled())
                        return;

                    $this->getPlugin()->sendMessageWithDistance(
                        $ev->getMessage(),
                        $ev->getInfo(),
                        $player
                    );
                break;

            case $this->getParam('items.pradio', 336):
                    $group = Group::getGroupByPlayer($player);
                    $this->createMessagesForm($player, $group->messages);
                break;

            case $this->getParam('items.fastCmd', 266):
                    $cmd = HLCore::getInstance()->commands->get(Group::getGroupByPlayer($player)->code);
                    if(!$cmd)
                        return;

                    $form = HLCore::getFormApi()->createSimpleForm(function($player, $data) {
                        self::$instance->fastCmdFormHandler($player, $data);
                    });
                    $form->setTitle($cmd['title']);

                    $this->selectCommand[$player->getName()] = $cmd['cmd'];

                    foreach($cmd['cmd'] as $cmdInfo) {
                        $button = new Button($cmdInfo['text']);
                        if(isset($cmdInfo['image']))
                            $button->addImage(Button::IMAGE_TYPE_URL, $cmdInfo['image']);
                        $form->addButton($button);
                    }

                    $form->sendForm($player);
                break;

        }
    }

    public function onPlayerDeath(PlayerDeathEvent $event) {
        $this->death[$event->getPlayer()->getName()] = 1;

        $drops = $event->getDrops();
        for($i = 0; $i < count($drops); $i++)
            if($drops[$i]->getId() == $this->getParam('CIDcard.id')) {
                unset($drops[$i]);
                break;
            }

        $event->setDrops($drops);
    }

    public function onBlockBreak(BlockBreakEvent $event) {
        if(Ban::isPlayerBanned($event->getPlayer()))
            $event->setCancelled(true);
    }

    public function onExplode(EntityExplodeEvent $event) {
        $event->setBlockList([]);
    }

    public function onCommand(PlayerCommandPreprocessEvent $event) {
        if(Ban::isPlayerBanned($event->getPlayer()))
            $event->setCancelled(true);
    }

    public static function staticSimpleFormHandler(Player $player, array $data) {
        self::$instance->simpleFormHandler($player, $data);
    }

    public function simpleFormHandler(Player $player, array $data) {
        $nickname = mb_strtolower($player->getName());
        $selectGroup = $data[0];
        if(isset($this->selectGroup[$nickname]) && isset($this->selectGroup[$nickname]['groups'][$selectGroup])) {
            /* @var Group $group */
            $group = $this->selectGroup[$nickname]['groups'][$selectGroup];

            if(Group::isGroupExists($player, $group)) {
                if($this->getPlugin()->isGroupLimit($group)) {
                    $player->sendMessage($this->getMessage('group.limit', [
                        $group->getName(),
                    ]));
                    return;
                }

                $player->setHealth(20);
                $spawn = $group->getSpawn();
                if(!$spawn)
                    $spawn = $player->level->getSpawnLocation();
                $player->teleport($spawn);
                aSkinSaver::getInstance()->setSkin($player, $group->hp[20]);
                $this->getPlugin()->setGroup($player, $group);
                return;
            }

            if(EconomyManager::getInstance()->getMoney($player) < $group->price) {
                $player->sendMessage($this->selectGroup[$nickname]['text']['money']);
                return;
            }

            if(Exp::getExp($player) < $group->exp) {
                $player->sendMessage($this->selectGroup[$nickname]['text']['exp']);
                return;
            }

            if($this->getPlugin()->isGroupLimit($group)) {
                $player->sendMessage($this->getMessage('group.limit', [
                    $group->getName(),
                ]));
                return;
            }

            $player->setHealth(20);
            $spawn = $group->getSpawn();
            if(!$spawn)
                $spawn = $player->level->getSpawnLocation();
            $player->teleport($spawn);

            EconomyManager::getInstance()->reduceMoney($player, $group->price);

            HLCore::getInstance()->addGroupByPlayer($player, $group);
            HLCore::getInstance()->setGroup($player, $group);
            aSkinSaver::getInstance()->setSkin($player, $group->hp[20]);

            $player->sendMessage(str_replace('{group}', $group->getName(),
                $this->selectGroup[$nickname]['text']['success'])
            );

            unset($this->selectGroup[$nickname]);
        }
    }

    public static function messagesStaticFormHandler($player, $data) {
        self::$instance->messagesFormHandler($player, $data);
    }

    public function messagesFormHandler(Player $player, array $data) {
        $message = $data[0];
        $message = $this->selectMessage[$player->getName()][$message]['text'];
        $message = $this->getParam('channel.radio') . $message;
        unset($this->selectMessage[$player->getName()]);

        $ev = new PlayerChatEvent(
            $player,
            $this->getParam('param.radioColor') . $message
        );
        $this->getPlugin()->getServer()->getPluginManager()->callEvent($ev);
        if(!$ev->isCancelled())
            HLCore::getInstance()->sendMessageWithDistance($ev->getFormat(), null, $player);
    }

    /**
     * @return HLCore
     */
    public function getPlugin(): HLCore {
        return $this->plugin;
    }

    public function fastCmdFormHandler(Player $player, array $data) {
        $data = $data[0];
        if(!isset($this->selectCommand[$player->getName()][$data]))
            return;

        $data = $this->selectCommand[$player->getName()][$data];
        unset($this->selectCommand[$player->getName()]);

        Server::getInstance()->dispatchCommand($player, $data['cmd']);
    }

    private function createFormSelectGroup(Player $player, array $npc) {
        $nickname = mb_strtolower($player->getName());

        $form = HLCore::getFormApi()->createSimpleForm(function($player, $data) {
            HLEventListener::staticSimpleFormHandler($player, $data);
        });
        $form->setTitle($npc['title']);

        $this->selectGroup[$nickname] = [
            'groups' => [],
            'text' => $npc['answer'],
        ];

        foreach($npc['groups'] as $desc) {
            $group = new Group($desc['groupName']);

            $this->selectGroup[$nickname]['groups'][] = $group;

            $text = $npc['format'];
            $text = str_replace([
                '&', '{text}', '{price}', '{exp}', '{isBought}',
            ], [
                '§', $desc['text'], $group->price, $group->exp,
                Group::isGroupExists($nickname, $group)
                    ? $npc['isBought']['true'] : $npc['isBought']['false']
            ], $text);
            $text = str_replace("\\n", "\n", $text);

            $button = new Button($text);
            if($desc['image'] && strlen($desc['image']) > 12)
                $button->addImage(Button::IMAGE_TYPE_URL, $desc['image']);

            $form->addButton($button);
        }

        $form->sendForm($player);
    }

    private function createMessagesForm(Player $player, array $messages, string $title = '') {
        $form = HLCore::getFormApi()->createSimpleForm(function($player, $data) {
            HLEventListener::messagesStaticFormHandler($player, $data);
        });
        $form->setTitle($title);

        $this->selectMessage[$player->getName()] = $messages;

        foreach($messages as $message)
            $form->addButton(new Button($message['title']));

        $form->sendForm($player);
    }

    /**
     * @param Player $player
     */
    public function createItems(Player $player) {
        $this->getPlugin()->createItems($player);
    }

    /**
     * @param Inventory $inv
     * @param Item|int $item
     */
    public function addItem(&$inv, $item) {
        $this->getPlugin()->addItem($inv, $item);
    }

    /**
     * @param Inventory $inv
     * @param int $itemId
     * @return bool
     */
    public function isItemExist(Inventory &$inv, int $itemId): bool {
        return $this->getPlugin()->isItemExist($inv, $itemId);
    }

    /**
     * @param  String     $node
     * @param  mixed|null $default
     * @return mixed
     */
    public function getParam($node, $default = null) {
        return $this->getPlugin()->getParam($node, $default);
    }

    public function getMessage($node, $vars = []) {
        return $this->getPlugin()->getMessage($node, $vars);
    }

}