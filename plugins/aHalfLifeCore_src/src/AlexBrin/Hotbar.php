<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 14.10.2017
 * Time: 14:51
 */

namespace AlexBrin;


use AlexBrin\utils\Ban;
use AlexBrin\utils\EconomyManager;
use AlexBrin\utils\Exp;
use AlexBrin\utils\Group;
use pocketmine\scheduler\PluginTask;

class Hotbar extends PluginTask {

    public function onRun(int $currentTick) {
        /* @var HLCore $plugin */
        $plugin = $this->getOwner();
        $online = count($this->getOwner()->getServer()->getOnlinePlayers());
        $maxOnline = $this->getOwner()->getServer()->getMaxPlayers();
        foreach($this->getOwner()->getServer()->getOnlinePlayers() as $player) {
            $text = $plugin->hotbar[mb_strtolower($player->getName())];

            $text = str_replace('{group}', Group::getGroupByPlayer($player)->getName(), $text);
            $text = str_replace('{exp}', Exp::getExp($player), $text);
            $text = str_replace('{money}', EconomyManager::getInstance()->getMoney($player), $text);
            $text = str_replace('{online}', $online, $text);
            $text = str_replace('{maxOnline}', $maxOnline, $text);
            if(Ban::isPlayerBanned($player)) {
                $ban = Ban::findByPlayer($player);
                $text = str_replace('{payday}', $ban['time'], $text);
                $text = str_replace('{reason}', $ban['reason'], $text);
                $text = str_replace('{player}', $ban['player'], $text);
            }
            $text = str_replace('\n', "\n", $text);

            $player->sendTip($text);
        }
    }
}