<?php
/**
 * Класс для работы выплаты (PayDay)
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin;

use AlexBrin\utils\Ban;
use AlexBrin\utils\EconomyManager;
use AlexBrin\utils\Exp;
use AlexBrin\utils\Group;
use pocketmine\scheduler\PluginTask;

class PayDay extends PluginTask {

    /**
     * Actions to execute when run
     *
     * @param $currentTick
     *
     * @return void
     */
    public function onRun(int $currentTick) {
        $eco = EconomyManager::getInstance();
        foreach($this->getOwner()->getServer()->getOnlinePlayers() as $player) {

            if(!Ban::isPlayerBanned($player)) {
                $group = $this->getOwner()->getGroupByPlayer($player);

                $eco->addMoney($player, $group->pay);
                Exp::addExp($player);
            } else {
                Ban::updateBan($player);
                $ban = Ban::findByPlayer($player);
                if(is_array($ban) && $ban['time'] < 1)
                    Ban::removePlayer($player);
            }

        }

        if($this->getParam('payday.broadcast', true))
            HLCore::sendBroadcast($this->getMessage('broadcast.payday'));
    }

    /**
     * @param $node
     * @param null $default
     * @return mixed
     */
    public function getParam($node, $default = null) {
        return HLCore::getInstance()->getParam($node, $default);
    }

    /**
     * @param $node
     * @param array $vars
     * @return mixed
     */
    public function getMessage($node, $vars = []) {
        return HLCore::getInstance()->getMessage($node, $vars);
    }
}