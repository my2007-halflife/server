<?php

/**
 * Команда ban
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\commands;

use AlexBrin\HLCore;
use AlexBrin\utils\Ban;
use pocketmine\command\CommandSender;
use pocketmine\Server;

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 18.09.2017
 * Time: 10:54
 */
class BanCommand extends Command {

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        $player = array_shift($args);
        if(!$player) {
            $sender->sendMessage($this->getUsage());
            return true;
        }

        $time = (int) array_shift($args);
        if(!$time) {
            $sender->sendMessage($this->getUsage());
            return true;
        }
        if($time > 60)
            $time = 60;

        $reason = implode(' ', $args);
        if(strlen($reason) < 1)
            $reason = 'Причина не указана';


//        $player = Server::getInstance()->getPlayer($player);
//        if(!$player)
//            $player = Server::getInstance()->getOfflinePlayer($player);
//        if(!$player) {
//            $sender->sendMessage('§e[HalfLife] Этого игрока никогда не было на сервере');
//            return true;
//        }

        $nickname = mb_strtolower($player);

        Ban::addPlayer($player, $reason, $time, $sender);

        $sender->sendMessage("§a[HalfLife] Игрок $nickname заблокирован на $time PayDay. Причина: $reason");

        if($this->getParam('command.ban.broadcast', true))
            HLCore::sendBroadcast(
                $this->getMessage('broadcast.commands.ban', [$sender->getName(), $nickname, $time, $reason])
            );

        return true;
    }
}