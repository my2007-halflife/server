<?php
/**
 * Абстрактный класс. Описание общего функционала
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\commands;

use AlexBrin\HLCore;

abstract class Command extends \pocketmine\command\Command {

    public function __construct(string $name = '', string $description = "", string $permission = '', string $usageMessage = null, array $aliases = []) {
        parent::__construct($name, $description, $usageMessage, $aliases);
        $this->setPermission($permission);
    }

    protected function getMessage($node, $vars = []) {
        return HLCore::getInstance()->getMessage($node, $vars);
    }

    protected function getParam($node, $default = null) {
        return HLCore::getInstance()->getParam($node, $default);
    }

}