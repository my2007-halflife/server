<?php
/**
 * Команда создания NPC
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\commands;

use AlexBrin\HLCore;
use pocketmine\command\CommandSender;
use pocketmine\Player;

class CreateNpcCommand extends Command {

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        if(!$sender instanceof Player) {
            $sender->sendMessage('§cOnly for Players');
            return true;
        }

        $name = implode(' ', $args);
        if(strlen($name) < 1) {
            $sender->sendMessage($this->getUsage());
            return true;
        }

        HLCore::getInstance()->createNPC(
            $sender->getLocation(),
            $sender->getSkin()->getSkinData(),
            $sender->getSkin()->getSkinId(),
            $name
        );

        HLCore::getInstance()->npc->set(str_replace(' ', '_', $name), [
            'name' => $name,
            'title' => 'Бот ' . $name,
            'answer' => [
                'success' => 'Отлично! Ты получил группу {group}',
                'buy' => 'Ты уже купил эту группу',
                'money' => 'У тебя недостаточно денег',
                'exp' => 'У тебя недостаточно опыта',
            ],
            'format' => '{text} ({isBought})\nЦена: {price} | EXP: {exp}',
            'isBought' => [
                'true' => '§aКуплено§8',
                'false' => '§eМожно купить§8'
            ],
            'groups' => [
                'new' => [
                    'groupName' => 'new',
                    'text' => 'Сообщение об этой группе',
                    'image' => null,
                ]
            ]
        ]);
        HLCore::getInstance()->npc->save();

        $sender->sendMessage('§a[HalfLife] NCP создан §7(§d' . $name . '§7)');

        return true;
    }

}