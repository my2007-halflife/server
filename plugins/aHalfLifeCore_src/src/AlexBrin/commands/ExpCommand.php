<?php
/**
 * Команда работы с опытом
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\commands;


use AlexBrin\utils\Exp;
use pocketmine\command\CommandSender;

class ExpCommand extends Command {

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {

        switch(array_shift($args)) {

            case 'my':
                    $sender->sendMessage($this->getMessage('exp.info.my', [Exp::getExp($sender)]));
                break;

            case 'help':
                    $sender->sendMessage($this->help());
                break;

            case 'add':
                    $player = array_shift($args);
                    if(!$player) {
                        $sender->sendMessage($this->help());
                        return true;
                    }

                    $exp = (int) array_shift($args);
                    if(!$exp) {
                        $sender->sendMessage($this->help());
                        return true;
                    }

                    Exp::addExp($player, $exp);
                    $sender->sendMessage("[HalfLife Exp] Игроку $player добавлен опыт: $exp");
                break;

            case 'remove':
                    $player = array_shift($args);
                    if(!$player) {
                        $sender->sendMessage($this->help());
                        return true;
                    }

                    $exp = (int) array_shift($args);
                    if(!$exp) {
                        $sender->sendMessage($this->help());
                        return true;
                    }

                    Exp::removeExp($player, $exp);
                    $sender->sendMessage("[HalfLife Exp] У игрока $player отнят опыт: $exp");
                break;

            case 'set':
                    $player = array_shift($args);
                    if(!$player) {
                        $sender->sendMessage($this->help());
                        return true;
                    }

                    $exp = (int) array_shift($args);
                    if(!$exp) {
                        $sender->sendMessage($this->help());
                        return true;
                    }

                    Exp::setExp($player, $exp);
                    $sender->sendMessage("[HalfLife Exp] Игроку $player установлен опыт: $exp");
                break;

            case 'info':
                    $player = mb_strtolower(array_shift($args));
                    if(!$player /*|| mb_strtolower($sender->getName()) == $player*/) {
                        $sender->sendMessage($this->getMessage('exp.info.my', [Exp::getExp($player)]));
                        return true;
                    }

                    if(!$sender->hasPermission('halflife.command.exp.info.otherPlayers')) {
                        $sender->sendMessage($this->getMessage('exp.forbidden'));
                        return true;
                    }

                    $sender->sendMessage(
                        $this->getMessage(
                            'exp.info.other',
                            [
                                Exp::getExp($player),
                                $player,
                            ]
                        )
                    );
                break;

            default:
                    $sender->sendMessage($this->getMessage('exp.info.my', [Exp::getExp($sender)]));
                break;

        }

        return true;
    }

    private function help() {
        $help  = '[HalfLife Exp] Помощь:' . "\n";
        $help  .= '/exp add <игрок> <опыт> - добавить опыт' . "\n";
        $help  .= '/exp remove <игрок> <опыт> - отнять опыт' . "\n";
        $help  .= '/exp set <игрок> <опыт> - установить опыт' . "\n";
        $help  .= '/exp info [player] - узнать свой (или чужой) опыт';

        return $help;
    }
}