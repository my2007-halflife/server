<?php
/**
 * Команда установки точки спавна для группы
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\commands;
use AlexBrin\HLCore;
use pocketmine\command\CommandSender;
use pocketmine\Player;

class GroupSpawnCommand extends Command {

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        if(!$sender instanceof Player) {
            $sender->sendMessage('§cOnly for players!');
            return true;
        }

        $groupCode = array_shift($args);
        if(!$groupCode) {
            $sender->sendMessage($this->getUsage());
            return true;
        }

        if(!HLCore::getInstance()->groups->exists($groupCode)) {
            $sender->sendMessage('§eГруппы не существует');
            return true;
        }

        $pos = $sender->getPosition();
        var_dump($pos);
        HLCore::getInstance()->setGroupSpawn($groupCode, $pos);
        $sender->sendMessage("§aТочка спавна группы $groupCode установлена на x{$pos->x} y{$pos->y} z{$pos->z}");

        return true;
    }
}