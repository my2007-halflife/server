<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 01.11.2017
 * Time: 16:27
 */

namespace AlexBrin\commands;


use pocketmine\command\CommandSender;
use pocketmine\Player;

class MyPosCommand extends Command {

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        if(!$sender instanceof Player) {
            $sender->sendMessage('§cOnly for Players!');
            return true;
        }

        $sender->sendMessage("X{$sender->x} Y{$sender->y} Z{$sender->z}");

        return true;
    }
}