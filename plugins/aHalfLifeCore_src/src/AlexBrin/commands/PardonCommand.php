<?php
/**
 * Команда для разбана
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\commands;

use AlexBrin\HLCore;
use AlexBrin\utils\Ban;
use pocketmine\command\CommandSender;
use pocketmine\Server;

class PardonCommand extends Command {

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        $player = array_shift($args);
        if(!$player) {
            $sender->sendMessage($this->getUsage());
            return true;
        }

        if(!Ban::isPlayerBanned($player)) {
            $sender->sendMessage($this->getMessage('ban.banNotEx', [$player]));
            return true;
        }

        $player = Server::getInstance()->getPlayer($player);
//        if(!$player) {
//            $sender->sendMessage('§e[HalfLife] Игрока нет на сервере');
//            return true;
//        }

        Ban::removePlayer($player, $sender);

        $sender->sendMessage("§a[HalfLife] Игрок {$player} разблокирован");

        if($this->getParam('command.pardon.broadcast', true))
            HLCore::sendBroadcast(
                $this->getMessage('broadcast.commands.pardon', [$sender->getName(), $player])
            );

        $player->sendMessage($this->getMessage('ban.pardon'));

        return true;
    }
}