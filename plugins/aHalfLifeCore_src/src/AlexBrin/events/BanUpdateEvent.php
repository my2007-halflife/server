<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 14.10.2017
 * Time: 15:55
 */

namespace AlexBrin\events;


use pocketmine\event\Cancellable;
use pocketmine\event\player\PlayerEvent;
use pocketmine\Player;
use pocketmine\Server;

class BanUpdateEvent extends PlayerEvent implements Cancellable {

    /**
     * @var int
     */
    protected $oldTime;
    /**
     * @var int
     */
    protected $newTime;

    public static $handlerList;

    public function __construct($player, int $oldTime, int $newTime) {
        $this->player = $player instanceof Player ? $player : Server::getInstance()->getPlayer($player)
            ?? Server::getInstance()->getOfflinePlayer($player);
        $this->oldTime = $oldTime;
        $this->newTime = $newTime;
    }

    public function getPlayer(): Player {
        return $this->player;
    }

    public function getOldTime(): int {
        return $this->oldTime;
    }

    public function getNewTime(): int {
        return $this->newTime;
    }

    public function setNewTime(int $newTime) {
        $this->newTime = $newTime;
    }

}