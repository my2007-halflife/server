<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 05.11.2017
 * Time: 14:19
 */

namespace AlexBrin\events;

use pocketmine\command\CommandSender;
use pocketmine\event\Cancellable;
use pocketmine\event\Event;
use pocketmine\OfflinePlayer;

class OfflinePlayerBanEvent extends Event implements Cancellable {

    /**
     * @var CommandSender
     */
    protected $sender;
    /**
     * @var int
     */
    protected $time;
    /**
     * @var string
     */
    protected $reason;

    /**
     * @var OfflinePlayer
     */
    protected $player;

    public static $handlerList;

    public function __construct(CommandSender $sender, OfflinePlayer $player, int $time, string $reason) {
        $this->sender = $sender;
        $this->player = $player;
        $this->time = $time;
        $this->reason = $reason;
    }

    public function getSender(): CommandSender {
        return $this->sender;
    }

    public function getPlayer(): OfflinePlayer {
        return $this->player;
    }

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time) {
        $this->time = $time;
    }

    public function getReason(): string {
        return $this->reason;
    }

    public function setReason(string $reason = '') {
        $this->reason = $reason;
    }

}