<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.11.2017
 * Time: 11:49
 */

namespace AlexBrin\events;


use pocketmine\command\CommandSender;
use pocketmine\OfflinePlayer;

class OfflinePlayerPardonEvent {

    /**
     * @var CommandSender
     */
    protected $sender;

    /**
     * @var OfflinePlayer
     */
    protected $player;

    public static $handlerList;

    public function __construct(CommandSender $sender, OfflinePlayer $player) {
        $this->sender = $sender;
        $this->player = $player;
    }

    public function getSender(): CommandSender {
        return $this->sender;
    }

    public function getPlayer(): OfflinePlayer {
        return $this->player;
    }

}