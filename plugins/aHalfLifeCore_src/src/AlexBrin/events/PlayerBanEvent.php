<?php

namespace AlexBrin\events;

use pocketmine\command\CommandSender;
use pocketmine\event\Cancellable;
use pocketmine\event\player\PlayerEvent;
use pocketmine\OfflinePlayer;
use pocketmine\Player;
use pocketmine\Server;

class PlayerBanEvent extends PlayerEvent implements Cancellable {

    /**
     * @var CommandSender
     */
    protected $sender;
    /**
     * @var int
     */
    protected $time;
    /**
     * @var string
     */
    protected $reason;

    public static $handlerList;

    public function __construct(CommandSender $sender, Player $player, int $time, string $reason) {
        $this->sender = $sender;
        $this->player = $player;
        $this->time = $time;
        $this->reason = $reason;
    }

    public function getSender(): CommandSender {
        return $this->sender;
    }

    public function getPlayer(): Player {
        return $this->player;
    }

    public function getTime(): int {
        return $this->time;
    }

    public function setTime(int $time) {
        $this->time = $time;
    }

    public function getReason(): string {
        return $this->reason;
    }

    public function setReason(string $reason = '') {
        $this->reason = $reason;
    }

}