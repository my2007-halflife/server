<?php

namespace AlexBrin\events;

use pocketmine\command\CommandSender;
use pocketmine\event\Cancellable;
use pocketmine\event\player\PlayerEvent;
use pocketmine\Player;
use pocketmine\Server;

class PlayerPardonEvent extends PlayerEvent implements Cancellable {

    /**
     * @var CommandSender
     */
    protected $sender;

    public static $handlerList;

    public function __construct(CommandSender $sender, Player $player) {
        $this->sender = $sender;
        $this->player = $player;
    }

    public function getSender(): CommandSender {
        return $this->sender;
    }

    public function getPlayer(): Player {
        return $this->player;
    }

}