<?php
/**
 * Событие показывания CID карты
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\events;

use AlexBrin\HLCore;
use AlexBrin\utils\Group;
use pocketmine\event\Cancellable;
use pocketmine\event\player\PlayerEvent;
use pocketmine\Player;

class PlayerShowCIDCardEvent extends PlayerEvent implements Cancellable {

    /**
     * @var string $message
     * @var string $info
     */
    private $message,
            $info;

    /** @var Group $group */
    private $group;

    public static $handlerList;

    public function __construct(Player $player, Group $group) {
        $this->player = $player;
        $this->group = $group;

        $this->message = HLCore::getInstance()->getMessage('CIDcard.show', [$player->getName()]);
        $this->info = HLCore::getInstance()->getMessage('CIDcard.info', [
            $group->getName(),
            $group->getLoyalty()
        ]);
    }

    public function getMessage(): string {
        return $this->message;
    }

    public function setMessage(String $message): void {
        $this->message = $message;
    }

    public function getInfo(): string {
        return $this->info;
    }

    public function setInfo(String $info): void {
        $this->info = $info;
    }

    public function getGroupName(): string {
        return $this->getGroup()->getName();
    }

    public function getGroupShortName(): string {
        return $this->getGroup()->getShort();
    }

    public function getLoyalty(): string {
        return $this->getGroup()->getLoyalty();
    }

    public function getGroup(): Group {
        return $this->group;
    }

}