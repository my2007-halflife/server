<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 14.10.2017
 * Time: 15:06
 */

namespace AlexBrin\sound;


use pocketmine\level\sound\GenericSound;
use pocketmine\math\Vector3;
use pocketmine\network\mcpe\protocol\LevelEventPacket;

class AnvilBreakSound extends GenericSound {

    public function __construct(Vector3 $pos) {
        parent::__construct($pos, LevelEventPacket::EVENT_SOUND_ANVIL_BREAK);
    }

}