<?php
/**
 * Звук телепорта эндермена
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\sound;

use pocketmine\level\sound\GenericSound;
use pocketmine\math\Vector3;
use pocketmine\network\mcpe\protocol\LevelEventPacket;

class TeleportEndermanSound extends GenericSound {

    public function __construct(Vector3 $pos) {
        parent::__construct($pos, LevelEventPacket::EVENT_SOUND_PORTAL);
    }

}