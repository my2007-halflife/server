<?php
/**
 * Класс для работы с банами
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\utils;

use AlexBrin\aSkinSaver;
use AlexBrin\events\BanUpdateEvent;
use AlexBrin\events\OfflinePlayerBanEvent;
use AlexBrin\events\OfflinePlayerPardonEvent;
use AlexBrin\events\PlayerBanEvent;
use AlexBrin\events\PlayerPardonEvent;
use AlexBrin\HLCore;
use pocketmine\command\CommandSender;
use pocketmine\command\ConsoleCommandSender;
use pocketmine\event\player\PlayerEvent;
use pocketmine\OfflinePlayer;
use pocketmine\Player;
use pocketmine\Server;

class Ban {

    /**
     * @param Player|OfflinePlayer|string $nickname
     * @param string $reason
     * @param int $time
     * @param CommandSender|null $sender
     */
    public static function addPlayer($nickname, string $reason, int $time, CommandSender $sender = null) {
        if(!$sender)
            $sender = new ConsoleCommandSender();

        $player = Server::getInstance()->getPlayer($nickname);
        if(!$player instanceof Player)
            $player = Server::getInstance()->getOfflinePlayer($nickname);

        $banGroup = HLCore::getInstance()->getParam('defaultValues.banGroup', 'banGroup');

        if($player instanceof PlayerEvent)
            $ev = new PlayerBanEvent($sender, $player, $time, $reason);
        else
            $ev = new OfflinePlayerBanEvent($sender, $player, $time, $reason);
        HLCore::callEvent($ev);
        if($ev->isCancelled())
            return;

        $player->getInventory()->clearAll();

        $nickname = mb_strtolower($player->getName());

        HLCore::sUpdateHotbar($player, HLCore::getInstance()->getParam('hotbar.ban'));

        HLCore::getInstance()->setGroup($nickname, $banGroup);
        self::changeSkin($player, new Group($banGroup));

        HLCore::getInstance()->bans->setNested($nickname, [
            'player' => $sender->getName(),
            'reason' => $ev->getReason(),
            'time' => $ev->getTime(),
        ]);

        HLCore::getInstance()->bans->save();
    }

    /**
     * @param Player|string $player
     * @param int $minus
     */
    public static function updateBan($player, $minus = 1) {
        if(!$player instanceof Player)
            $player = $player->getName();
        $nickname = mb_strtolower($player->getName());

        if(!Ban::isPlayerBanned($player))
            return;

        $time = HLCore::getInstance()->bans->getNested($nickname.'.time');
        $ev = new BanUpdateEvent($player, $time, $time - $minus);
        HLCore::callEvent($ev);
        if($ev->isCancelled())
            return;

        HLCore::getInstance()->bans->setNested($nickname.'.time', $ev->getNewTime());
        HLCore::getInstance()->bans->save(true);
    }

    /**
     * @param Player|String $nickname
     * @param CommandSender|null $sender
     */
    public static function removePlayer($nickname, CommandSender $sender = null) {
        if(!$sender)
            $sender = new ConsoleCommandSender();

        $player = Server::getInstance()->getPlayer($nickname);

        if(!$player)
            $player = Server::getInstance()->getOfflinePlayer($nickname);

        if(!$player) {
            if($sender)
                $sender->sendMessage('[HalfLife] Этого игрока никогда не было на сервере');
            return;
        }

        $nickname = mb_strtolower($player->getName());

        $plugin = HLCore::getInstance();

        if($plugin->bans->exists($nickname)) {
            $group = Group::isGroupExists($nickname, $plugin->getParam('defaultValues.secondGroup'))
                        ? $plugin->getParam('defaultValues.secondGroup')
                        : $plugin->getParam('defaultValues.group');

            if($player instanceof Player)
                $ev = new PlayerPardonEvent($sender, $player);
            else
                $ev = new OfflinePlayerPardonEvent($sender, $player);
            HLCore::callEvent($ev);
            if($ev->isCancelled())
                return;

            HLCore::getInstance()->createItems($player);

            HLCore::getInstance()->setGroup($player, new Group($group));

            self::changeSkin($player, $group);

            HLCore::sUpdateHotbar($nickname, HLCore::getInstance()->getParam('hotbar.player'));

            HLCore::getInstance()->bans->remove($nickname);
            HLCore::getInstance()->bans->save(true);
        }
    }

    /**
     * @param  Player|String $player
     * @return bool
     */
    public static function isPlayerBanned($player): bool {
        if(self::findByPlayer($player))
            return true;
        return false;
    }

    /**
     * @param $player
     * @return array|null
     */
    public static function findByPlayer($player) {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        return HLCore::getInstance()->bans->get($player, null);
    }

    private static function changeSkin($player, $group) {
        if(!$player instanceof Player)
            $player = HLCore::getInstance()->getServer()->getPlayer($player);

        if(!$player)
            return;

        if(!$group instanceof Group)
            $group = new Group($group);

        $player->setHealth(20);
        aSkinSaver::getInstance()->setSkin($player, $group->hp[20]);
    }

}