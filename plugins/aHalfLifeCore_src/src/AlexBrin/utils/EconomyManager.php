<?php
/**
 * Класс для работы с экономиками
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\utils;

use AlexBrin\aEconomy;
use pocketmine\command\ConsoleCommandSender;
use pocketmine\plugin\Plugin;

class EconomyManager {

    private static $instance;

    /**
     * @var aEconomy
     */
    private $eco;

    public function __construct(Plugin $plugin) {
        $pManager = $plugin->getServer()->getPluginManager();
        $this->eco = $pManager->getPlugin('aEconomy') ?? $pManager->getPlugin("EconomyAPI")
            ?? $pManager->getPlugin("PocketMoney") ?? $pManager->getPlugin("MassiveEconomy") ?? null;

        if($this->eco)
            $plugin->getLogger()->notice('Найден плагин на экономику: ' . $this->eco->getName());
        else
            $plugin->getLogger()->warning('Плагин на экономику отсутствует');

        self::$instance = &$this;
    }

    public function addMoney($player, float $amount): bool {
        if(!$this->eco)
            return false;

        $this->eco->addMoney($player, $amount);
        return true;
    }

    public function reduceMoney($player, float $amount): bool {
        if(!$this->eco)
            return false;

        $this->eco->reduceMoney($player, $amount);
        return true;
    }

    public function setMoney($player, float $amount): bool {
        if(!$this->eco)
            return false;

        $this->eco->setMoney($player, $amount);
        return true;
    }

    /**
     * @param  $player
     * @return float
     */
    public function getMoney($player): float {
        if(!$this->eco)
            return false;

        switch(mb_strtolower($this->eco->getName())) {
            case 'economyapi':
                $balance = $this->eco->myMoney($player);
                break;

            default:
                $balance = $this->eco->getMoney($player);
        }

        return $balance;
    }

    public static function getInstance(): EconomyManager {
        return self::$instance;
    }

}