<?php
/**
 * Класс для работы с опытом
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\utils;

use AlexBrin\HLCore;
use pocketmine\Player;

class Exp {

    /**
     * @param Player|string $player
     * @param int $exp
     */
    public static function addExp($player, int $exp = 1) {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        HLCore::getInstance()->players->setNested("$player.exp",
            Exp::getExp($player) + $exp
        );
        HLCore::getInstance()->players->save();
    }

    /**
     * @param Player|string $player
     * @param int $exp
     */
    public static function removeExp($player, $exp = 1) {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);


        $currentExp = HLCore::getInstance()->players->getNested("$player.exp");

        HLCore::getInstance()->players->setNested("$player.exp", $currentExp - $exp);
        HLCore::getInstance()->players->save();
    }

    public static function getExp($player): int {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        return HLCore::getInstance()->players->getNested("$player.exp", 0);
    }

    public static function setExp($player, $exp) {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        HLCore::getInstance()->players->setNested("$player.exp", $exp);
        HLCore::getInstance()->players->save();
    }

}