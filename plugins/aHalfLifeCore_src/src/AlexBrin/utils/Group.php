<?php
/**
 * Класс для работы с группами
 * Код защищен авторским правом
 * © Alex Brin, 2017
 */

namespace AlexBrin\utils;

use AlexBrin\aSkinSaver;
use AlexBrin\HLCore;
use pocketmine\IPlayer;
use pocketmine\math\Vector3;
use pocketmine\Player;
use pocketmine\Server;

class Group {

    private $name,
            $loyalty;

    public  $price,
            $code,
            $exp,
            $pay,
            $limit,
            $hp,
            $messages;

    public function __construct($name) {
        if(is_array($name)) {
            $this->name = $name['name'];
            $this->short = $name['code'];
            $this->price = $name['price'];
            $this->code = $name['code'];
            $this->exp = $name['exp'];
            $this->pay = $name['pay'];
            $this->limit = $name['limit'];
            $this->hp = $name['hp'];
            $this->messages = $name['messages'];
            $this->spawn = $name['spawn'] ?? null;
            return;
        }

        $groups = &HLCore::getInstance()->groups;

        if(!($group = $groups->get($name))) {
            $this->name = 'WTF';
            $this->loyalty = 'WTF';
            $this->price = 0;
            $this->code = 'UNKNOWN';
            $this->exp = 0;
            $this->pay = 0;
            $this->limit = 0;
            $this->hp = [];
            $this->messages = [];
            $this->spawn = null;
            return;
        }
        unset($groups);

        $this->name = $group['name'];
        $this->loyalty = $group['loyalty'];
        $this->price = $group['price'];
        $this->code = $group['code'];
        $this->exp = $group['exp'];
        $this->pay = $group['pay'];
        $this->limit = $group['limit'];
        $this->hp = $group['hp'];
        $this->messages = $group['messages'];
        $this->spawn = $group['spawn'] ?? null;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getLoyalty(): string {
        return $this->loyalty;
    }

    public function isAccessToFracChat(): bool {
        $code = explode('.', $this->code)[0];
        return in_array($code, HLCore::getInstance()->getParam('params.accessLocalChat'));
    }

    /**
     * @return Vector3
     */
    public function getSpawn() {
        if(!isset($this->spawn[0]['x']))
            return null;

        return new Vector3(
            $this->spawn[0]['x'],
            $this->spawn[0]['y'],
            $this->spawn[0]['z']
        );
    }

    public static function getGroupsByPlayer($player): array {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        return HLCore::getInstance()->players->getNested($player.'.groups', []);
    }

    /**
     * @param Player|string $player
     * @return Group
     */
    public static function getGroupByPlayer($player): Group {
        if($player instanceof Player)
            $player = $player->getName();
        $player = mb_strtolower($player);

        return new Group(HLCore::getInstance()->players->getNested($player.'.group'));
    }

    public static function isGroupExists($player, $group): bool {
        if($group instanceof Group)
            $group = $group->code;

        return in_array($group, self::getGroupsByPlayer($player));
    }

    public static function getAllGroups(): array {
        $groups = [];
        foreach(HLCore::getInstance()->groups->getAll() as $group)
            $groups[] = new Group($group);

        return $groups;
    }

    /**
     * @param Group|string $group
     * @return array
     */
    public static function getOnlinePlayers($group): array {
        if(!$group instanceof Group)
            $group = new Group($group);

        $players = [];
        foreach(Server::getInstance()->getOnlinePlayers() as $player)
            if(self::getGroupByPlayer($player)->getName() == $group->getName())
                $players[] = $player;

        return $players;
    }

    /**
     * @param IPlayer|string $player
     * @param $group
     * @return bool
     */
    public static function changeGroup($player, $group): bool {
        if(!$player instanceof IPlayer)
            $player = Server::getInstance()->getPlayer($player);

        if($group instanceof Group)
            $group = $group->code;

        if(!$player)
            return false;

        $player->getInventory()->clearAll();
        HLCore::getInstance()->createItems($player);

        $group = HLCore::getInstance()->getPurePerms()->getGroup($group);
        HLCore::getInstance()->getPurePerms()->getUserDataMgr()->setGroup($player, $group, null);
        return true;
    }

}