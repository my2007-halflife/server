<?php

namespace AlexBrin;

use AlexBrin\commands\SkinListCommand;
use AlexBrin\commands\SkinSaverCommand;
use AlexBrin\commands\SetSkinCommand;
use pocketmine\entity\Skin;
use pocketmine\plugin\PluginBase;
use pocketmine\Player;

class aSkinSaver extends PluginBase {

    public static $instance;

	public function onEnable() {
		$f = $this->getDataFolder();
		if(!is_dir($f))
			@mkdir($f);

		$this->getServer()->getCommandMap()->register('saveskin',
            new SkinSaverCommand(
                'saveskin',
                'Skin saver',
                'skinsaver.commands.saveskin',
                'Use: /saveskin <player> <nickname>',
                ['ss', 'saveskin', 'sskin']
            )
        );

        $this->getServer()->getCommandMap()->register('setskin',
            new SetSkinCommand(
                'setskin',
                '',
                'skinsaver.commands.setskin',
                'Use: /setskin <player> <skinName>',
                ['sets']
            )
        );

        $this->getServer()->getCommandMap()->register('skinlist',
            new SkinListCommand(
                'skinlist',
                '',
                'skinsaver.commands.skinlist',
                'Use: /skinlist',
                ['sl', 'skins', 'slist']
            )
        );

		self::$instance = &$this;
	}

    /**
     * @param $skinName
     * @param $skinData
     */
	public function saveSkin($skinName, $skinData) {
	    if($skinData instanceof Skin)
	        $skinData = $skinData->getSkinData();

        file_put_contents($this->getDataFolder().$skinName.".skin", $skinData);
    }

    /**
     * @param $skinName
     * @return Skin
     */
    public function getSkin($skinName) {
        if(!$this->isSkinExists($skinName))
            return null;

        return new Skin($skinName, file_get_contents($this->getDataFolder().$skinName.'.skin'));
    }

    /**
     * @param Player $player
     * @param Skin|string $skin
     * @return bool
     */
    public function setSkin(Player $player, $skin): bool {
        if(!$skin instanceof Skin)
            $skin = $this->getSkin($skin);

        if(!$skin)
            return false;

        $player->setSkin($skin);
        $player->sendSkin($this->getServer()->getOnlinePlayers());

        return true;
    }

    public function isSkinExists($skinName): bool {
        return file_exists($this->getDataFolder() . $skinName . '.skin');
    }

    /**
     * @return aSkinSaver
     */
    public static function getInstance() {
	    return self::$instance;
    }

}

?>