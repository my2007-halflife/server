<?php

namespace AlexBrin\commands;

use AlexBrin\aSkinSaver;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;
use pocketmine\entity\Skin;
use pocketmine\Player;

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 21.09.2017
 * Time: 0:31
 */
class SetSkinCommand extends Command {

    public function __construct($name, $description = '', $permission = '', $usageMessage = null, $aliases = []) {
        parent::__construct($name, $description, $usageMessage, $aliases);
        $this->setPermission($permission);
    }

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        if(!$sender instanceof Player) {
            $sender->sendMessage('§cOnly for Players');
            return true;
        }

        $player = array_shift($args);
        if(!$player) {
            if($sender instanceof Player)
                $player = $sender->getPlayer();
            else {
                $sender->sendMessage("§cOnly for Players");
                return true;
            }
        }
        else {
            $player = aSkinSaver::getInstance()->getServer()->getPlayer($player);
            if(!$player) {
                $sender->sendMessage("§ePlayer offline");
                return true;
            }
        }

        $skinName = array_shift($args);
        if(!$skinName) {
            $sender->sendMessage($this->getUsage());
            return true;
        }

        $skin = aSkinSaver::getInstance()->getSkin($skinName);
        if(!$skin) {
            $sender->sendMessage('§cSkin not exist');
            return true;
        }

        aSkinSaver::getInstance()->setSkin($player, $skin);

        $sender->sendMessage("§aSkin changed");

        return true;
    }
}