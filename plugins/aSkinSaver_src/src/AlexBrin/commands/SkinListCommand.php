<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 27.09.2017
 * Time: 22:00
 */

namespace AlexBrin\commands;


use AlexBrin\aSkinSaver;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;

class SkinListCommand extends Command {

    public function __construct($name, $description = '', $permission = '', $usageMessage = null, $aliases = []) {
        parent::__construct($name, $description, $usageMessage, $aliases);
        $this->setPermission($permission);
    }

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        $skins = scandir(aSkinSaver::getInstance()->getDataFolder());
        unset($skins[0], $skins[1]);

        $message = [];
        foreach($skins as $skin) {
            $message[] = $skin;
        }

        $sender->sendMessage("§aSkins (" . count($skins) ."): " . implode(',', $message));

        return true;
    }
}