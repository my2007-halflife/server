<?php

namespace AlexBrin\commands;

use AlexBrin\aSkinSaver;
use pocketmine\command\Command;
use pocketmine\command\CommandSender;

class SkinSaverCommand extends Command {

    public function __construct($name, $description = '', $permission = '', $usageMessage = null, $aliases = []) {
        parent::__construct($name, $description, $usageMessage, $aliases);
        $this->setPermission($permission);
    }

    /**
     * @param CommandSender $sender
     * @param string $commandLabel
     * @param string[] $args
     *
     * @return mixed
     */
    public function execute(CommandSender $sender, string $commandLabel, array $args): bool {
        $player = array_shift($args);
        if(!$player) {
            $sender->sendMessage($this->getUsage());
            return true;
        }

        $player = aSkinSaver::getInstance()->getServer()->getPlayer($player);
        if(!$player)
            $player = aSkinSaver::getInstance()->getServer()->getOfflinePlayer($player);

        $skinName = array_shift($args);
        if(!$skinName) {
            $sender->sendMessage($this->getUsage());
            return true;
        }

        if(aSkinSaver::getInstance()->isSkinExists($skinName)) {
            $sender->sendMessage("§eSkin already exists");
            return true;
        }

        if(aSkinSaver::getInstance()->saveSkin($skinName, $player->getSkin()->getSkinData()))
            $sender->sendMessage('Skin saved: ' . $skinName . '.skin');
        else
            $sender->sendMessage("error");

        return true;
    }
}