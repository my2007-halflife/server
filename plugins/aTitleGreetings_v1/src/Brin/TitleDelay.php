<?php

	namespace Brin;

	use pocketmine\scheduler\PluginTask;

	class TitleDelay extends PluginTask {

        /**
         * @var \pocketmine\Player
         */
	    private $player;

	    private $title,
                $subtitle;

		public function __construct(aTG $plugin, \pocketmine\Player $player, $title, $subtitle) {
			parent::__construct($plugin);
			$this->player   = $player;
			$this->title    = $title;
			$this->subtitle = $subtitle;
		}

		public function onRun(int $tick) {
		    if(aTG::$pmmp)
		        $this->player->addTitle($this->title, $this->subtitle);
		    else
    			$this->player->sendTitle($this->title, $this->subtitle);
		}

	}

?>