<?php

	namespace Brin;

	use pocketmine\plugin\PluginBase;
	use pocketmine\utils\Config;

	use pocketmine\event\Listener;
	use pocketmine\event\player\PlayerJoinEvent;

	use Brin\utils\bUtils;
	use Brin\TitleDelay;

	class aTG extends PluginBase implements Listener {

        /**
         * @var Config
         */
		private $config;

		public static $pmmp = false;

		public function onEnable() {
			$f = $this->getDataFolder();
			if(!is_dir($f))
				@mkdir($f);

			$this->saveResource('config.yml');
			$this->config = new Config($f.'config.yml', Config::YAML);
			$this->getServer()->getPluginManager()->registerEvents($this, $this);

			if(method_exists("\pocketmine\Player", "addTitle"))
			    self::$pmmp = true;
		}

		public function onPlayerJoin(PlayerJoinEvent $event) {
			$player = $event->getPlayer();

			if($this->config->get('defaultMessage', true))
				$event->setJoinMessage(null);

			$replace  = [$player->getName()];
			$title    = $this->getMessage('message.title',    $replace);
			$subtitle = $this->getMessage('message.subtitle', $replace);
			$this->getServer()->getScheduler()->scheduleDelayedTask(new TitleDelay($this, $player, $title, $subtitle), 20);
		}

		public function getMessage(string $node, array $vars = [], bool $declination = false) {
			$message = $this->config->getNested($node);

			$i = 0;
			foreach($vars as $var) {
				$message = str_replace("%var$i%", $var, $message);
				$i++;
			}

			if($declination)
				$message = bUtils::getDeclination($message);

			return $message;
		}

	}

?>