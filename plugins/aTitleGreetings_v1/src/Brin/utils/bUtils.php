<?php

	namespace Brin\utils;

	class bUtils {

		/**
		 * @param int   $num - количество
		 * @param array $words - 3 варианта склонения. Именительный, Родительный, Родительный множественный
		 * 
		 * @return String
		 * 
		 */
		public static function declination(int $num, array $words): string {
			$num = $num % 100;
			if($num > 10 && $num < 20)
				return $words[2];
			
			$num = $num % 10;
			switch($num) {
				case 1:
						return $words[0];
					break;

				case 2:
				case 3:
				case 4:
						return $words[1];
					break;

				default:
						return $words[2];
			}

		}

		/**
		 * @param String $msg
		 * Сообщение вида "любой текст @число_вариант1|вариант2|вариант3@"
		 * 
		 * @return String
		 */
		public static function getDeclination($msg) {
			preg_match("/@(\d+)_(.*)@/", $msg, $variants);
			if(count($variants) == 3) {
				$word = self::declination($variants[1], explode('|', $variants[2], 3));
				$msg = preg_replace("/@(\d+)_(.*)@/", $variants[1].' '.$word, $msg);
			}
			return $msg;
		}

	}

?>